/**
 * Browse_12_2Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_12_2Response  implements java.io.Serializable {
    private org.tempuri.wynaws.wsother.Browse_12_2ResponseBrowse_12_2Result browse_12_2Result;

    public Browse_12_2Response() {
    }

    public Browse_12_2Response(
           org.tempuri.wynaws.wsother.Browse_12_2ResponseBrowse_12_2Result browse_12_2Result) {
           this.browse_12_2Result = browse_12_2Result;
    }


    /**
     * Gets the browse_12_2Result value for this Browse_12_2Response.
     * 
     * @return browse_12_2Result
     */
    public org.tempuri.wynaws.wsother.Browse_12_2ResponseBrowse_12_2Result getBrowse_12_2Result() {
        return browse_12_2Result;
    }


    /**
     * Sets the browse_12_2Result value for this Browse_12_2Response.
     * 
     * @param browse_12_2Result
     */
    public void setBrowse_12_2Result(org.tempuri.wynaws.wsother.Browse_12_2ResponseBrowse_12_2Result browse_12_2Result) {
        this.browse_12_2Result = browse_12_2Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_12_2Response)) return false;
        Browse_12_2Response other = (Browse_12_2Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browse_12_2Result==null && other.getBrowse_12_2Result()==null) || 
             (this.browse_12_2Result!=null &&
              this.browse_12_2Result.equals(other.getBrowse_12_2Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowse_12_2Result() != null) {
            _hashCode += getBrowse_12_2Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_12_2Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_12_2Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browse_12_2Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Browse_12_2Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">>Browse_12_2Response>Browse_12_2Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
