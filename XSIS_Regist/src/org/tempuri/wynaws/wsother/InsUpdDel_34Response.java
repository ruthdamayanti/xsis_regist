/**
 * InsUpdDel_34Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class InsUpdDel_34Response  implements java.io.Serializable {
    private boolean insUpdDel_34Result;

    public InsUpdDel_34Response() {
    }

    public InsUpdDel_34Response(
           boolean insUpdDel_34Result) {
           this.insUpdDel_34Result = insUpdDel_34Result;
    }


    /**
     * Gets the insUpdDel_34Result value for this InsUpdDel_34Response.
     * 
     * @return insUpdDel_34Result
     */
    public boolean isInsUpdDel_34Result() {
        return insUpdDel_34Result;
    }


    /**
     * Sets the insUpdDel_34Result value for this InsUpdDel_34Response.
     * 
     * @param insUpdDel_34Result
     */
    public void setInsUpdDel_34Result(boolean insUpdDel_34Result) {
        this.insUpdDel_34Result = insUpdDel_34Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InsUpdDel_34Response)) return false;
        InsUpdDel_34Response other = (InsUpdDel_34Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.insUpdDel_34Result == other.isInsUpdDel_34Result();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isInsUpdDel_34Result() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InsUpdDel_34Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">InsUpdDel_34Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insUpdDel_34Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "InsUpdDel_34Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
