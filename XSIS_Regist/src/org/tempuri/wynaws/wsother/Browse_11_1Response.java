/**
 * Browse_11_1Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_11_1Response  implements java.io.Serializable {
    private org.tempuri.wynaws.wsother.Browse_11_1ResponseBrowse_11_1Result browse_11_1Result;

    public Browse_11_1Response() {
    }

    public Browse_11_1Response(
           org.tempuri.wynaws.wsother.Browse_11_1ResponseBrowse_11_1Result browse_11_1Result) {
           this.browse_11_1Result = browse_11_1Result;
    }


    /**
     * Gets the browse_11_1Result value for this Browse_11_1Response.
     * 
     * @return browse_11_1Result
     */
    public org.tempuri.wynaws.wsother.Browse_11_1ResponseBrowse_11_1Result getBrowse_11_1Result() {
        return browse_11_1Result;
    }


    /**
     * Sets the browse_11_1Result value for this Browse_11_1Response.
     * 
     * @param browse_11_1Result
     */
    public void setBrowse_11_1Result(org.tempuri.wynaws.wsother.Browse_11_1ResponseBrowse_11_1Result browse_11_1Result) {
        this.browse_11_1Result = browse_11_1Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_11_1Response)) return false;
        Browse_11_1Response other = (Browse_11_1Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browse_11_1Result==null && other.getBrowse_11_1Result()==null) || 
             (this.browse_11_1Result!=null &&
              this.browse_11_1Result.equals(other.getBrowse_11_1Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowse_11_1Result() != null) {
            _hashCode += getBrowse_11_1Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_11_1Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_11_1Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browse_11_1Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Browse_11_1Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">>Browse_11_1Response>Browse_11_1Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
