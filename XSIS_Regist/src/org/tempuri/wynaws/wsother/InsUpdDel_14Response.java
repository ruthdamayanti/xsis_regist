/**
 * InsUpdDel_14Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class InsUpdDel_14Response  implements java.io.Serializable {
    private boolean insUpdDel_14Result;

    public InsUpdDel_14Response() {
    }

    public InsUpdDel_14Response(
           boolean insUpdDel_14Result) {
           this.insUpdDel_14Result = insUpdDel_14Result;
    }


    /**
     * Gets the insUpdDel_14Result value for this InsUpdDel_14Response.
     * 
     * @return insUpdDel_14Result
     */
    public boolean isInsUpdDel_14Result() {
        return insUpdDel_14Result;
    }


    /**
     * Sets the insUpdDel_14Result value for this InsUpdDel_14Response.
     * 
     * @param insUpdDel_14Result
     */
    public void setInsUpdDel_14Result(boolean insUpdDel_14Result) {
        this.insUpdDel_14Result = insUpdDel_14Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InsUpdDel_14Response)) return false;
        InsUpdDel_14Response other = (InsUpdDel_14Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.insUpdDel_14Result == other.isInsUpdDel_14Result();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isInsUpdDel_14Result() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InsUpdDel_14Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">InsUpdDel_14Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insUpdDel_14Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "InsUpdDel_14Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
