/**
 * Browse_5_4Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_5_4Response  implements java.io.Serializable {
    private org.tempuri.wynaws.wsother.Browse_5_4ResponseBrowse_5_4Result browse_5_4Result;

    public Browse_5_4Response() {
    }

    public Browse_5_4Response(
           org.tempuri.wynaws.wsother.Browse_5_4ResponseBrowse_5_4Result browse_5_4Result) {
           this.browse_5_4Result = browse_5_4Result;
    }


    /**
     * Gets the browse_5_4Result value for this Browse_5_4Response.
     * 
     * @return browse_5_4Result
     */
    public org.tempuri.wynaws.wsother.Browse_5_4ResponseBrowse_5_4Result getBrowse_5_4Result() {
        return browse_5_4Result;
    }


    /**
     * Sets the browse_5_4Result value for this Browse_5_4Response.
     * 
     * @param browse_5_4Result
     */
    public void setBrowse_5_4Result(org.tempuri.wynaws.wsother.Browse_5_4ResponseBrowse_5_4Result browse_5_4Result) {
        this.browse_5_4Result = browse_5_4Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_5_4Response)) return false;
        Browse_5_4Response other = (Browse_5_4Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browse_5_4Result==null && other.getBrowse_5_4Result()==null) || 
             (this.browse_5_4Result!=null &&
              this.browse_5_4Result.equals(other.getBrowse_5_4Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowse_5_4Result() != null) {
            _hashCode += getBrowse_5_4Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_5_4Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_5_4Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browse_5_4Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Browse_5_4Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">>Browse_5_4Response>Browse_5_4Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
