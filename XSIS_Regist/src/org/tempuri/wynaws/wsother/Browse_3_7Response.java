/**
 * Browse_3_7Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_3_7Response  implements java.io.Serializable {
    private org.tempuri.wynaws.wsother.Browse_3_7ResponseBrowse_3_7Result browse_3_7Result;

    public Browse_3_7Response() {
    }

    public Browse_3_7Response(
           org.tempuri.wynaws.wsother.Browse_3_7ResponseBrowse_3_7Result browse_3_7Result) {
           this.browse_3_7Result = browse_3_7Result;
    }


    /**
     * Gets the browse_3_7Result value for this Browse_3_7Response.
     * 
     * @return browse_3_7Result
     */
    public org.tempuri.wynaws.wsother.Browse_3_7ResponseBrowse_3_7Result getBrowse_3_7Result() {
        return browse_3_7Result;
    }


    /**
     * Sets the browse_3_7Result value for this Browse_3_7Response.
     * 
     * @param browse_3_7Result
     */
    public void setBrowse_3_7Result(org.tempuri.wynaws.wsother.Browse_3_7ResponseBrowse_3_7Result browse_3_7Result) {
        this.browse_3_7Result = browse_3_7Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_3_7Response)) return false;
        Browse_3_7Response other = (Browse_3_7Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browse_3_7Result==null && other.getBrowse_3_7Result()==null) || 
             (this.browse_3_7Result!=null &&
              this.browse_3_7Result.equals(other.getBrowse_3_7Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowse_3_7Result() != null) {
            _hashCode += getBrowse_3_7Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_3_7Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_3_7Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browse_3_7Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Browse_3_7Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">>Browse_3_7Response>Browse_3_7Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
