/**
 * Browse_7_4Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_7_4Response  implements java.io.Serializable {
    private org.tempuri.wynaws.wsother.Browse_7_4ResponseBrowse_7_4Result browse_7_4Result;

    public Browse_7_4Response() {
    }

    public Browse_7_4Response(
           org.tempuri.wynaws.wsother.Browse_7_4ResponseBrowse_7_4Result browse_7_4Result) {
           this.browse_7_4Result = browse_7_4Result;
    }


    /**
     * Gets the browse_7_4Result value for this Browse_7_4Response.
     * 
     * @return browse_7_4Result
     */
    public org.tempuri.wynaws.wsother.Browse_7_4ResponseBrowse_7_4Result getBrowse_7_4Result() {
        return browse_7_4Result;
    }


    /**
     * Sets the browse_7_4Result value for this Browse_7_4Response.
     * 
     * @param browse_7_4Result
     */
    public void setBrowse_7_4Result(org.tempuri.wynaws.wsother.Browse_7_4ResponseBrowse_7_4Result browse_7_4Result) {
        this.browse_7_4Result = browse_7_4Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_7_4Response)) return false;
        Browse_7_4Response other = (Browse_7_4Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browse_7_4Result==null && other.getBrowse_7_4Result()==null) || 
             (this.browse_7_4Result!=null &&
              this.browse_7_4Result.equals(other.getBrowse_7_4Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowse_7_4Result() != null) {
            _hashCode += getBrowse_7_4Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_7_4Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_7_4Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browse_7_4Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Browse_7_4Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">>Browse_7_4Response>Browse_7_4Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
