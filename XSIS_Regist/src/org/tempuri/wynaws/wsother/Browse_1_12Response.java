/**
 * Browse_1_12Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_1_12Response  implements java.io.Serializable {
    private org.tempuri.wynaws.wsother.Browse_1_12ResponseBrowse_1_12Result browse_1_12Result;

    public Browse_1_12Response() {
    }

    public Browse_1_12Response(
           org.tempuri.wynaws.wsother.Browse_1_12ResponseBrowse_1_12Result browse_1_12Result) {
           this.browse_1_12Result = browse_1_12Result;
    }


    /**
     * Gets the browse_1_12Result value for this Browse_1_12Response.
     * 
     * @return browse_1_12Result
     */
    public org.tempuri.wynaws.wsother.Browse_1_12ResponseBrowse_1_12Result getBrowse_1_12Result() {
        return browse_1_12Result;
    }


    /**
     * Sets the browse_1_12Result value for this Browse_1_12Response.
     * 
     * @param browse_1_12Result
     */
    public void setBrowse_1_12Result(org.tempuri.wynaws.wsother.Browse_1_12ResponseBrowse_1_12Result browse_1_12Result) {
        this.browse_1_12Result = browse_1_12Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_1_12Response)) return false;
        Browse_1_12Response other = (Browse_1_12Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browse_1_12Result==null && other.getBrowse_1_12Result()==null) || 
             (this.browse_1_12Result!=null &&
              this.browse_1_12Result.equals(other.getBrowse_1_12Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowse_1_12Result() != null) {
            _hashCode += getBrowse_1_12Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_1_12Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_1_12Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browse_1_12Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Browse_1_12Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">>Browse_1_12Response>Browse_1_12Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
