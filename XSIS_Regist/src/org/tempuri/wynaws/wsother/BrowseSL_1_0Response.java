/**
 * BrowseSL_1_0Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class BrowseSL_1_0Response  implements java.io.Serializable {
    private org.tempuri.wynaws.wsother.BrowseSL_1_0ResponseBrowseSL_1_0Result browseSL_1_0Result;

    public BrowseSL_1_0Response() {
    }

    public BrowseSL_1_0Response(
           org.tempuri.wynaws.wsother.BrowseSL_1_0ResponseBrowseSL_1_0Result browseSL_1_0Result) {
           this.browseSL_1_0Result = browseSL_1_0Result;
    }


    /**
     * Gets the browseSL_1_0Result value for this BrowseSL_1_0Response.
     * 
     * @return browseSL_1_0Result
     */
    public org.tempuri.wynaws.wsother.BrowseSL_1_0ResponseBrowseSL_1_0Result getBrowseSL_1_0Result() {
        return browseSL_1_0Result;
    }


    /**
     * Sets the browseSL_1_0Result value for this BrowseSL_1_0Response.
     * 
     * @param browseSL_1_0Result
     */
    public void setBrowseSL_1_0Result(org.tempuri.wynaws.wsother.BrowseSL_1_0ResponseBrowseSL_1_0Result browseSL_1_0Result) {
        this.browseSL_1_0Result = browseSL_1_0Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BrowseSL_1_0Response)) return false;
        BrowseSL_1_0Response other = (BrowseSL_1_0Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browseSL_1_0Result==null && other.getBrowseSL_1_0Result()==null) || 
             (this.browseSL_1_0Result!=null &&
              this.browseSL_1_0Result.equals(other.getBrowseSL_1_0Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowseSL_1_0Result() != null) {
            _hashCode += getBrowseSL_1_0Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BrowseSL_1_0Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">BrowseSL_1_0Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browseSL_1_0Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "BrowseSL_1_0Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">>BrowseSL_1_0Response>BrowseSL_1_0Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
