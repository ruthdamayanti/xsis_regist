/**
 * WsOther.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public interface WsOther extends javax.xml.rpc.Service {
    public java.lang.String getWsOtherSoapAddress();

    public org.tempuri.wynaws.wsother.WsOtherSoap getWsOtherSoap() throws javax.xml.rpc.ServiceException;

    public org.tempuri.wynaws.wsother.WsOtherSoap getWsOtherSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
