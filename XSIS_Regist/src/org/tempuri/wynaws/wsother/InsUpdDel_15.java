/**
 * InsUpdDel_15.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class InsUpdDel_15  implements java.io.Serializable {
    private java.lang.String parameter1;

    private java.lang.String parameter2;

    private java.lang.String parameter3;

    private java.lang.String parameter4;

    private java.lang.String parameter5;

    private java.lang.String parameter6;

    private java.lang.String parameter7;

    private java.lang.String parameter8;

    private java.lang.String parameter9;

    private java.lang.String parameter10;

    private java.lang.String parameter11;

    private java.lang.String parameter12;

    private java.lang.String parameter13;

    private java.lang.String parameter14;

    private java.lang.String parameter15;

    private java.lang.String OUI;

    private java.lang.String OPW;

    private java.lang.String ODS;

    private java.lang.String oraPkg;

    public InsUpdDel_15() {
    }

    public InsUpdDel_15(
           java.lang.String parameter1,
           java.lang.String parameter2,
           java.lang.String parameter3,
           java.lang.String parameter4,
           java.lang.String parameter5,
           java.lang.String parameter6,
           java.lang.String parameter7,
           java.lang.String parameter8,
           java.lang.String parameter9,
           java.lang.String parameter10,
           java.lang.String parameter11,
           java.lang.String parameter12,
           java.lang.String parameter13,
           java.lang.String parameter14,
           java.lang.String parameter15,
           java.lang.String OUI,
           java.lang.String OPW,
           java.lang.String ODS,
           java.lang.String oraPkg) {
           this.parameter1 = parameter1;
           this.parameter2 = parameter2;
           this.parameter3 = parameter3;
           this.parameter4 = parameter4;
           this.parameter5 = parameter5;
           this.parameter6 = parameter6;
           this.parameter7 = parameter7;
           this.parameter8 = parameter8;
           this.parameter9 = parameter9;
           this.parameter10 = parameter10;
           this.parameter11 = parameter11;
           this.parameter12 = parameter12;
           this.parameter13 = parameter13;
           this.parameter14 = parameter14;
           this.parameter15 = parameter15;
           this.OUI = OUI;
           this.OPW = OPW;
           this.ODS = ODS;
           this.oraPkg = oraPkg;
    }


    /**
     * Gets the parameter1 value for this InsUpdDel_15.
     * 
     * @return parameter1
     */
    public java.lang.String getParameter1() {
        return parameter1;
    }


    /**
     * Sets the parameter1 value for this InsUpdDel_15.
     * 
     * @param parameter1
     */
    public void setParameter1(java.lang.String parameter1) {
        this.parameter1 = parameter1;
    }


    /**
     * Gets the parameter2 value for this InsUpdDel_15.
     * 
     * @return parameter2
     */
    public java.lang.String getParameter2() {
        return parameter2;
    }


    /**
     * Sets the parameter2 value for this InsUpdDel_15.
     * 
     * @param parameter2
     */
    public void setParameter2(java.lang.String parameter2) {
        this.parameter2 = parameter2;
    }


    /**
     * Gets the parameter3 value for this InsUpdDel_15.
     * 
     * @return parameter3
     */
    public java.lang.String getParameter3() {
        return parameter3;
    }


    /**
     * Sets the parameter3 value for this InsUpdDel_15.
     * 
     * @param parameter3
     */
    public void setParameter3(java.lang.String parameter3) {
        this.parameter3 = parameter3;
    }


    /**
     * Gets the parameter4 value for this InsUpdDel_15.
     * 
     * @return parameter4
     */
    public java.lang.String getParameter4() {
        return parameter4;
    }


    /**
     * Sets the parameter4 value for this InsUpdDel_15.
     * 
     * @param parameter4
     */
    public void setParameter4(java.lang.String parameter4) {
        this.parameter4 = parameter4;
    }


    /**
     * Gets the parameter5 value for this InsUpdDel_15.
     * 
     * @return parameter5
     */
    public java.lang.String getParameter5() {
        return parameter5;
    }


    /**
     * Sets the parameter5 value for this InsUpdDel_15.
     * 
     * @param parameter5
     */
    public void setParameter5(java.lang.String parameter5) {
        this.parameter5 = parameter5;
    }


    /**
     * Gets the parameter6 value for this InsUpdDel_15.
     * 
     * @return parameter6
     */
    public java.lang.String getParameter6() {
        return parameter6;
    }


    /**
     * Sets the parameter6 value for this InsUpdDel_15.
     * 
     * @param parameter6
     */
    public void setParameter6(java.lang.String parameter6) {
        this.parameter6 = parameter6;
    }


    /**
     * Gets the parameter7 value for this InsUpdDel_15.
     * 
     * @return parameter7
     */
    public java.lang.String getParameter7() {
        return parameter7;
    }


    /**
     * Sets the parameter7 value for this InsUpdDel_15.
     * 
     * @param parameter7
     */
    public void setParameter7(java.lang.String parameter7) {
        this.parameter7 = parameter7;
    }


    /**
     * Gets the parameter8 value for this InsUpdDel_15.
     * 
     * @return parameter8
     */
    public java.lang.String getParameter8() {
        return parameter8;
    }


    /**
     * Sets the parameter8 value for this InsUpdDel_15.
     * 
     * @param parameter8
     */
    public void setParameter8(java.lang.String parameter8) {
        this.parameter8 = parameter8;
    }


    /**
     * Gets the parameter9 value for this InsUpdDel_15.
     * 
     * @return parameter9
     */
    public java.lang.String getParameter9() {
        return parameter9;
    }


    /**
     * Sets the parameter9 value for this InsUpdDel_15.
     * 
     * @param parameter9
     */
    public void setParameter9(java.lang.String parameter9) {
        this.parameter9 = parameter9;
    }


    /**
     * Gets the parameter10 value for this InsUpdDel_15.
     * 
     * @return parameter10
     */
    public java.lang.String getParameter10() {
        return parameter10;
    }


    /**
     * Sets the parameter10 value for this InsUpdDel_15.
     * 
     * @param parameter10
     */
    public void setParameter10(java.lang.String parameter10) {
        this.parameter10 = parameter10;
    }


    /**
     * Gets the parameter11 value for this InsUpdDel_15.
     * 
     * @return parameter11
     */
    public java.lang.String getParameter11() {
        return parameter11;
    }


    /**
     * Sets the parameter11 value for this InsUpdDel_15.
     * 
     * @param parameter11
     */
    public void setParameter11(java.lang.String parameter11) {
        this.parameter11 = parameter11;
    }


    /**
     * Gets the parameter12 value for this InsUpdDel_15.
     * 
     * @return parameter12
     */
    public java.lang.String getParameter12() {
        return parameter12;
    }


    /**
     * Sets the parameter12 value for this InsUpdDel_15.
     * 
     * @param parameter12
     */
    public void setParameter12(java.lang.String parameter12) {
        this.parameter12 = parameter12;
    }


    /**
     * Gets the parameter13 value for this InsUpdDel_15.
     * 
     * @return parameter13
     */
    public java.lang.String getParameter13() {
        return parameter13;
    }


    /**
     * Sets the parameter13 value for this InsUpdDel_15.
     * 
     * @param parameter13
     */
    public void setParameter13(java.lang.String parameter13) {
        this.parameter13 = parameter13;
    }


    /**
     * Gets the parameter14 value for this InsUpdDel_15.
     * 
     * @return parameter14
     */
    public java.lang.String getParameter14() {
        return parameter14;
    }


    /**
     * Sets the parameter14 value for this InsUpdDel_15.
     * 
     * @param parameter14
     */
    public void setParameter14(java.lang.String parameter14) {
        this.parameter14 = parameter14;
    }


    /**
     * Gets the parameter15 value for this InsUpdDel_15.
     * 
     * @return parameter15
     */
    public java.lang.String getParameter15() {
        return parameter15;
    }


    /**
     * Sets the parameter15 value for this InsUpdDel_15.
     * 
     * @param parameter15
     */
    public void setParameter15(java.lang.String parameter15) {
        this.parameter15 = parameter15;
    }


    /**
     * Gets the OUI value for this InsUpdDel_15.
     * 
     * @return OUI
     */
    public java.lang.String getOUI() {
        return OUI;
    }


    /**
     * Sets the OUI value for this InsUpdDel_15.
     * 
     * @param OUI
     */
    public void setOUI(java.lang.String OUI) {
        this.OUI = OUI;
    }


    /**
     * Gets the OPW value for this InsUpdDel_15.
     * 
     * @return OPW
     */
    public java.lang.String getOPW() {
        return OPW;
    }


    /**
     * Sets the OPW value for this InsUpdDel_15.
     * 
     * @param OPW
     */
    public void setOPW(java.lang.String OPW) {
        this.OPW = OPW;
    }


    /**
     * Gets the ODS value for this InsUpdDel_15.
     * 
     * @return ODS
     */
    public java.lang.String getODS() {
        return ODS;
    }


    /**
     * Sets the ODS value for this InsUpdDel_15.
     * 
     * @param ODS
     */
    public void setODS(java.lang.String ODS) {
        this.ODS = ODS;
    }


    /**
     * Gets the oraPkg value for this InsUpdDel_15.
     * 
     * @return oraPkg
     */
    public java.lang.String getOraPkg() {
        return oraPkg;
    }


    /**
     * Sets the oraPkg value for this InsUpdDel_15.
     * 
     * @param oraPkg
     */
    public void setOraPkg(java.lang.String oraPkg) {
        this.oraPkg = oraPkg;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InsUpdDel_15)) return false;
        InsUpdDel_15 other = (InsUpdDel_15) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.parameter1==null && other.getParameter1()==null) || 
             (this.parameter1!=null &&
              this.parameter1.equals(other.getParameter1()))) &&
            ((this.parameter2==null && other.getParameter2()==null) || 
             (this.parameter2!=null &&
              this.parameter2.equals(other.getParameter2()))) &&
            ((this.parameter3==null && other.getParameter3()==null) || 
             (this.parameter3!=null &&
              this.parameter3.equals(other.getParameter3()))) &&
            ((this.parameter4==null && other.getParameter4()==null) || 
             (this.parameter4!=null &&
              this.parameter4.equals(other.getParameter4()))) &&
            ((this.parameter5==null && other.getParameter5()==null) || 
             (this.parameter5!=null &&
              this.parameter5.equals(other.getParameter5()))) &&
            ((this.parameter6==null && other.getParameter6()==null) || 
             (this.parameter6!=null &&
              this.parameter6.equals(other.getParameter6()))) &&
            ((this.parameter7==null && other.getParameter7()==null) || 
             (this.parameter7!=null &&
              this.parameter7.equals(other.getParameter7()))) &&
            ((this.parameter8==null && other.getParameter8()==null) || 
             (this.parameter8!=null &&
              this.parameter8.equals(other.getParameter8()))) &&
            ((this.parameter9==null && other.getParameter9()==null) || 
             (this.parameter9!=null &&
              this.parameter9.equals(other.getParameter9()))) &&
            ((this.parameter10==null && other.getParameter10()==null) || 
             (this.parameter10!=null &&
              this.parameter10.equals(other.getParameter10()))) &&
            ((this.parameter11==null && other.getParameter11()==null) || 
             (this.parameter11!=null &&
              this.parameter11.equals(other.getParameter11()))) &&
            ((this.parameter12==null && other.getParameter12()==null) || 
             (this.parameter12!=null &&
              this.parameter12.equals(other.getParameter12()))) &&
            ((this.parameter13==null && other.getParameter13()==null) || 
             (this.parameter13!=null &&
              this.parameter13.equals(other.getParameter13()))) &&
            ((this.parameter14==null && other.getParameter14()==null) || 
             (this.parameter14!=null &&
              this.parameter14.equals(other.getParameter14()))) &&
            ((this.parameter15==null && other.getParameter15()==null) || 
             (this.parameter15!=null &&
              this.parameter15.equals(other.getParameter15()))) &&
            ((this.OUI==null && other.getOUI()==null) || 
             (this.OUI!=null &&
              this.OUI.equals(other.getOUI()))) &&
            ((this.OPW==null && other.getOPW()==null) || 
             (this.OPW!=null &&
              this.OPW.equals(other.getOPW()))) &&
            ((this.ODS==null && other.getODS()==null) || 
             (this.ODS!=null &&
              this.ODS.equals(other.getODS()))) &&
            ((this.oraPkg==null && other.getOraPkg()==null) || 
             (this.oraPkg!=null &&
              this.oraPkg.equals(other.getOraPkg())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getParameter1() != null) {
            _hashCode += getParameter1().hashCode();
        }
        if (getParameter2() != null) {
            _hashCode += getParameter2().hashCode();
        }
        if (getParameter3() != null) {
            _hashCode += getParameter3().hashCode();
        }
        if (getParameter4() != null) {
            _hashCode += getParameter4().hashCode();
        }
        if (getParameter5() != null) {
            _hashCode += getParameter5().hashCode();
        }
        if (getParameter6() != null) {
            _hashCode += getParameter6().hashCode();
        }
        if (getParameter7() != null) {
            _hashCode += getParameter7().hashCode();
        }
        if (getParameter8() != null) {
            _hashCode += getParameter8().hashCode();
        }
        if (getParameter9() != null) {
            _hashCode += getParameter9().hashCode();
        }
        if (getParameter10() != null) {
            _hashCode += getParameter10().hashCode();
        }
        if (getParameter11() != null) {
            _hashCode += getParameter11().hashCode();
        }
        if (getParameter12() != null) {
            _hashCode += getParameter12().hashCode();
        }
        if (getParameter13() != null) {
            _hashCode += getParameter13().hashCode();
        }
        if (getParameter14() != null) {
            _hashCode += getParameter14().hashCode();
        }
        if (getParameter15() != null) {
            _hashCode += getParameter15().hashCode();
        }
        if (getOUI() != null) {
            _hashCode += getOUI().hashCode();
        }
        if (getOPW() != null) {
            _hashCode += getOPW().hashCode();
        }
        if (getODS() != null) {
            _hashCode += getODS().hashCode();
        }
        if (getOraPkg() != null) {
            _hashCode += getOraPkg().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InsUpdDel_15.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">InsUpdDel_15"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter5");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter6");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter6"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter7");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter7"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter8");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter8"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter9");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter9"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter10");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter10"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter11");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter11"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter12");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter12"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter13");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter13"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter14");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter14"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter15");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter15"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OUI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OUI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OPW");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OPW"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ODS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "ODS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oraPkg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OraPkg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
