package org.tempuri.wynaws.wsother;

public class WsOtherSoapProxy implements org.tempuri.wynaws.wsother.WsOtherSoap {
  private String _endpoint = null;
  private org.tempuri.wynaws.wsother.WsOtherSoap wsOtherSoap = null;
  
  public WsOtherSoapProxy() {
    _initWsOtherSoapProxy();
  }
  
  public WsOtherSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initWsOtherSoapProxy();
  }
  
  private void _initWsOtherSoapProxy() {
    try {
      wsOtherSoap = (new org.tempuri.wynaws.wsother.WsOtherLocator()).getWsOtherSoap();
      if (wsOtherSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wsOtherSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wsOtherSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wsOtherSoap != null)
      ((javax.xml.rpc.Stub)wsOtherSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.wynaws.wsother.WsOtherSoap getWsOtherSoap() {
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap;
  }
  
//  public org.tempuri.wynaws.wsother.Browse_6_1Response browse_6(org.tempuri.wynaws.wsother.Browse_6_1 parameters) throws java.rmi.RemoteException{
//    if (wsOtherSoap == null)
//      _initWsOtherSoapProxy();
//    return wsOtherSoap.browse_6(parameters);
//  }
  
  public org.tempuri.wynaws.wsother.Browse_6_2Response browse_6(org.tempuri.wynaws.wsother.Browse_6_2 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_6(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_6_3Response browse_6(org.tempuri.wynaws.wsother.Browse_6_3 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_6(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_6_4Response browse_6(org.tempuri.wynaws.wsother.Browse_6_4 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_6(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_6_5Response browse_6(org.tempuri.wynaws.wsother.Browse_6_5 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_6(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_6_6Response browse_6(org.tempuri.wynaws.wsother.Browse_6_6 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_6(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_6_7Response browse_6(org.tempuri.wynaws.wsother.Browse_6_7 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_6(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_6_8Response browse_6(org.tempuri.wynaws.wsother.Browse_6_8 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_6(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_7_0Response browse_7(org.tempuri.wynaws.wsother.Browse_7_0 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_7(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_7_1Response browse_7(org.tempuri.wynaws.wsother.Browse_7_1 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_7(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_7_2Response browse_7(org.tempuri.wynaws.wsother.Browse_7_2 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_7(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_7_3Response browse_7(org.tempuri.wynaws.wsother.Browse_7_3 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_7(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_7_4Response browse_7(org.tempuri.wynaws.wsother.Browse_7_4 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_7(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_7_5Response browse_7(org.tempuri.wynaws.wsother.Browse_7_5 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_7(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_7_6Response browse_7(org.tempuri.wynaws.wsother.Browse_7_6 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_7(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_7_7Response browse_7(org.tempuri.wynaws.wsother.Browse_7_7 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_7(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_7_8Response browse_7(org.tempuri.wynaws.wsother.Browse_7_8 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_7(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_11_1Response browse_11(org.tempuri.wynaws.wsother.Browse_11_1 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_11(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_12_1Response browse_12(org.tempuri.wynaws.wsother.Browse_12_1 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_12(parameters);
  }
  
  public org.tempuri.wynaws.wsother.Browse_12_2Response browse_12(org.tempuri.wynaws.wsother.Browse_12_2 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_12(parameters);
  }
  
  public org.tempuri.wynaws.wsother.BrowseXML_1_0Response browseXML_1(org.tempuri.wynaws.wsother.BrowseXML_1_0 parameters) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browseXML_1(parameters);
  }
  
  public boolean insUpdDel_1(java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_1(parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_2(java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_2(parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_3(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_3(parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_4(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_4(parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_5(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_5(parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_6(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_6(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_7(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_7(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_8(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_8(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_9(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_9(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_10(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_10(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_11(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_11(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_12(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_12(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_13(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_13(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_14(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_14(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_15(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_15(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_16(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_16(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_17(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_17(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_18(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_18(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_19(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_19(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_20(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_20(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_21(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_21(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_22(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_22(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_23(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_23(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_24(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_24(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_25(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_25(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_26(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_26(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_27(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_27(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_28(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_28(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_29(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_29(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_30(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String parameter30, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_30(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, parameter30, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_31(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String parameter30, java.lang.String parameter31, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_31(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, parameter30, parameter31, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_32(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String parameter30, java.lang.String parameter31, java.lang.String parameter32, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_32(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, parameter30, parameter31, parameter32, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_33(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String parameter30, java.lang.String parameter31, java.lang.String parameter32, java.lang.String parameter33, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_33(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, parameter30, parameter31, parameter32, parameter33, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_34(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String parameter30, java.lang.String parameter31, java.lang.String parameter32, java.lang.String parameter33, java.lang.String parameter34, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_34(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, parameter30, parameter31, parameter32, parameter33, parameter34, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_35(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String parameter30, java.lang.String parameter31, java.lang.String parameter32, java.lang.String parameter33, java.lang.String parameter34, java.lang.String parameter35, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_35(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, parameter30, parameter31, parameter32, parameter33, parameter34, parameter35, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_36(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String parameter30, java.lang.String parameter31, java.lang.String parameter32, java.lang.String parameter33, java.lang.String parameter34, java.lang.String parameter35, java.lang.String parameter36, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_36(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, parameter30, parameter31, parameter32, parameter33, parameter34, parameter35, parameter36, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_37(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String parameter30, java.lang.String parameter31, java.lang.String parameter32, java.lang.String parameter33, java.lang.String parameter34, java.lang.String parameter35, java.lang.String parameter36, java.lang.String parameter37, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_37(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, parameter30, parameter31, parameter32, parameter33, parameter34, parameter35, parameter36, parameter37, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_38(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String parameter30, java.lang.String parameter31, java.lang.String parameter32, java.lang.String parameter33, java.lang.String parameter34, java.lang.String parameter35, java.lang.String parameter36, java.lang.String parameter37, java.lang.String parameter38, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_38(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, parameter30, parameter31, parameter32, parameter33, parameter34, parameter35, parameter36, parameter37, parameter38, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdDel_39(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String parameter16, java.lang.String parameter17, java.lang.String parameter18, java.lang.String parameter19, java.lang.String parameter20, java.lang.String parameter21, java.lang.String parameter22, java.lang.String parameter23, java.lang.String parameter24, java.lang.String parameter25, java.lang.String parameter26, java.lang.String parameter27, java.lang.String parameter28, java.lang.String parameter29, java.lang.String parameter30, java.lang.String parameter31, java.lang.String parameter32, java.lang.String parameter33, java.lang.String parameter34, java.lang.String parameter35, java.lang.String parameter36, java.lang.String parameter37, java.lang.String parameter38, java.lang.String parameter39, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdDel_39(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, parameter16, parameter17, parameter18, parameter19, parameter20, parameter21, parameter22, parameter23, parameter24, parameter25, parameter26, parameter27, parameter28, parameter29, parameter30, parameter31, parameter32, parameter33, parameter34, parameter35, parameter36, parameter37, parameter38, parameter39, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdBlob_3(java.lang.String parameter1, byte[] parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdBlob_3(parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdBlob_4(java.lang.String parameter1, java.lang.String parameter2, byte[] parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdBlob_4(parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdBlob_5(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, byte[] parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdBlob_5(parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public boolean insUpdBlob_6(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, byte[] parameter5, java.lang.String parameter6, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.insUpdBlob_6(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.BrowseSL_1_0ResponseBrowseSL_1_0Result browseSL_1(java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browseSL_1(OUI, OPW, ODS, oraPkg);
  }
  
  public java.lang.String helloWorld() throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.helloWorld();
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_1_0ResponseBrowse_Date_1_0Result browse_Date_1(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_1(DATE1, DATE2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_1_1ResponseBrowse_Date_1_1Result browse_Date_1(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_1(DATE1, DATE2, parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_1_2ResponseBrowse_Date_1_2Result browse_Date_1(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_1(DATE1, DATE2, parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_1_3ResponseBrowse_Date_1_3Result browse_Date_1(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_1(DATE1, DATE2, parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_1_4ResponseBrowse_Date_1_4Result browse_Date_1(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_1(DATE1, DATE2, parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_1_5ResponseBrowse_Date_1_5Result browse_Date_1(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_1(DATE1, DATE2, parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_2_0ResponseBrowse_Date_2_0Result browse_Date_2(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_2(DATE1, DATE2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_2_1ResponseBrowse_Date_2_1Result browse_Date_2(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_2(DATE1, DATE2, parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_2_2ResponseBrowse_Date_2_2Result browse_Date_2(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_2(DATE1, DATE2, parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_2_3ResponseBrowse_Date_2_3Result browse_Date_2(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_2(DATE1, DATE2, parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_2_4ResponseBrowse_Date_2_4Result browse_Date_2(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_2(DATE1, DATE2, parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_2_5ResponseBrowse_Date_2_5Result browse_Date_2(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_2(DATE1, DATE2, parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_3_0ResponseBrowse_Date_3_0Result browse_Date_3(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_3(DATE1, DATE2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_3_1ResponseBrowse_Date_3_1Result browse_Date_3(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_3(DATE1, DATE2, parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_3_2ResponseBrowse_Date_3_2Result browse_Date_3(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_3(DATE1, DATE2, parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_3_3ResponseBrowse_Date_3_3Result browse_Date_3(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_3(DATE1, DATE2, parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_3_4ResponseBrowse_Date_3_4Result browse_Date_3(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_3(DATE1, DATE2, parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_3_5ResponseBrowse_Date_3_5Result browse_Date_3(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_3(DATE1, DATE2, parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_4_0ResponseBrowse_Date_4_0Result browse_Date_4(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_4(DATE1, DATE2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_4_1ResponseBrowse_Date_4_1Result browse_Date_4(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_4(DATE1, DATE2, parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_4_2ResponseBrowse_Date_4_2Result browse_Date_4(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_4(DATE1, DATE2, parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_4_3ResponseBrowse_Date_4_3Result browse_Date_4(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_4(DATE1, DATE2, parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_4_4ResponseBrowse_Date_4_4Result browse_Date_4(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_4(DATE1, DATE2, parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_4_5ResponseBrowse_Date_4_5Result browse_Date_4(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_4(DATE1, DATE2, parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_5_0ResponseBrowse_Date_5_0Result browse_Date_5(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_5(DATE1, DATE2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_5_1ResponseBrowse_Date_5_1Result browse_Date_5(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_5(DATE1, DATE2, parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_5_2ResponseBrowse_Date_5_2Result browse_Date_5(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_5(DATE1, DATE2, parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_5_3ResponseBrowse_Date_5_3Result browse_Date_5(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_5(DATE1, DATE2, parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_5_4ResponseBrowse_Date_5_4Result browse_Date_5(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_5(DATE1, DATE2, parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_Date_5_5ResponseBrowse_Date_5_5Result browse_Date_5(java.util.Calendar DATE1, java.util.Calendar DATE2, java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_Date_5(DATE1, DATE2, parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_0ResponseBrowse_1_0Result browse_1(java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_1ResponseBrowse_1_1Result browse_1(java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_2ResponseBrowse_1_2Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_3ResponseBrowse_1_3Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_4ResponseBrowse_1_4Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_5ResponseBrowse_1_5Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_6ResponseBrowse_1_6Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_7ResponseBrowse_1_7Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_8ResponseBrowse_1_8Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_9ResponseBrowse_1_9Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_10ResponseBrowse_1_10Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_11ResponseBrowse_1_11Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_12ResponseBrowse_1_12Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_13ResponseBrowse_1_13Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_14ResponseBrowse_1_14Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_1_15ResponseBrowse_1_15Result browse_1(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String parameter9, java.lang.String parameter10, java.lang.String parameter11, java.lang.String parameter12, java.lang.String parameter13, java.lang.String parameter14, java.lang.String parameter15, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_1(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, parameter9, parameter10, parameter11, parameter12, parameter13, parameter14, parameter15, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_2_0ResponseBrowse_2_0Result browse_2(java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_2(OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_2_1ResponseBrowse_2_1Result browse_2(java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_2(parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_2_2ResponseBrowse_2_2Result browse_2(java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_2(parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_2_3ResponseBrowse_2_3Result browse_2(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_2(parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_2_4ResponseBrowse_2_4Result browse_2(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_2(parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_2_5ResponseBrowse_2_5Result browse_2(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_2(parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_2_6ResponseBrowse_2_6Result browse_2(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_2(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_2_7ResponseBrowse_2_7Result browse_2(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_2(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_2_8ResponseBrowse_2_8Result browse_2(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_2(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_3_0ResponseBrowse_3_0Result browse_3(java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_3(OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_3_1ResponseBrowse_3_1Result browse_3(java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_3(parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_3_2ResponseBrowse_3_2Result browse_3(java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_3(parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_3_3ResponseBrowse_3_3Result browse_3(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_3(parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_3_4ResponseBrowse_3_4Result browse_3(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_3(parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_3_5ResponseBrowse_3_5Result browse_3(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_3(parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_3_6ResponseBrowse_3_6Result browse_3(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_3(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_3_7ResponseBrowse_3_7Result browse_3(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_3(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_3_8ResponseBrowse_3_8Result browse_3(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_3(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_4_0ResponseBrowse_4_0Result browse_4(java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_4(OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_4_1ResponseBrowse_4_1Result browse_4(java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_4(parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_4_2ResponseBrowse_4_2Result browse_4(java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_4(parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_4_3ResponseBrowse_4_3Result browse_4(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_4(parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_4_4ResponseBrowse_4_4Result browse_4(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_4(parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_4_5ResponseBrowse_4_5Result browse_4(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_4(parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_4_6ResponseBrowse_4_6Result browse_4(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_4(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_4_7ResponseBrowse_4_7Result browse_4(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_4(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_4_8ResponseBrowse_4_8Result browse_4(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_4(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_5_0ResponseBrowse_5_0Result browse_5(java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_5(OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_5_1ResponseBrowse_5_1Result browse_5(java.lang.String parameter1, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_5(parameter1, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_5_2ResponseBrowse_5_2Result browse_5(java.lang.String parameter1, java.lang.String parameter2, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_5(parameter1, parameter2, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_5_3ResponseBrowse_5_3Result browse_5(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_5(parameter1, parameter2, parameter3, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_5_4ResponseBrowse_5_4Result browse_5(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_5(parameter1, parameter2, parameter3, parameter4, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_5_5ResponseBrowse_5_5Result browse_5(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_5(parameter1, parameter2, parameter3, parameter4, parameter5, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_5_6ResponseBrowse_5_6Result browse_5(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_5(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_5_7ResponseBrowse_5_7Result browse_5(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_5(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_5_8ResponseBrowse_5_8Result browse_5(java.lang.String parameter1, java.lang.String parameter2, java.lang.String parameter3, java.lang.String parameter4, java.lang.String parameter5, java.lang.String parameter6, java.lang.String parameter7, java.lang.String parameter8, java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_5(parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7, parameter8, OUI, OPW, ODS, oraPkg);
  }
  
  public org.tempuri.wynaws.wsother.Browse_6_0ResponseBrowse_6_0Result browse_6(java.lang.String OUI, java.lang.String OPW, java.lang.String ODS, java.lang.String oraPkg) throws java.rmi.RemoteException{
    if (wsOtherSoap == null)
      _initWsOtherSoapProxy();
    return wsOtherSoap.browse_6(OUI, OPW, ODS, oraPkg);
  }
  
  
}