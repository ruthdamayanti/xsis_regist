/**
 * Browse_4_6Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_4_6Response  implements java.io.Serializable {
    private org.tempuri.wynaws.wsother.Browse_4_6ResponseBrowse_4_6Result browse_4_6Result;

    public Browse_4_6Response() {
    }

    public Browse_4_6Response(
           org.tempuri.wynaws.wsother.Browse_4_6ResponseBrowse_4_6Result browse_4_6Result) {
           this.browse_4_6Result = browse_4_6Result;
    }


    /**
     * Gets the browse_4_6Result value for this Browse_4_6Response.
     * 
     * @return browse_4_6Result
     */
    public org.tempuri.wynaws.wsother.Browse_4_6ResponseBrowse_4_6Result getBrowse_4_6Result() {
        return browse_4_6Result;
    }


    /**
     * Sets the browse_4_6Result value for this Browse_4_6Response.
     * 
     * @param browse_4_6Result
     */
    public void setBrowse_4_6Result(org.tempuri.wynaws.wsother.Browse_4_6ResponseBrowse_4_6Result browse_4_6Result) {
        this.browse_4_6Result = browse_4_6Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_4_6Response)) return false;
        Browse_4_6Response other = (Browse_4_6Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browse_4_6Result==null && other.getBrowse_4_6Result()==null) || 
             (this.browse_4_6Result!=null &&
              this.browse_4_6Result.equals(other.getBrowse_4_6Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowse_4_6Result() != null) {
            _hashCode += getBrowse_4_6Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_4_6Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_4_6Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browse_4_6Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Browse_4_6Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">>Browse_4_6Response>Browse_4_6Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
