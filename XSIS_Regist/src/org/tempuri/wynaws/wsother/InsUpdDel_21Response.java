/**
 * InsUpdDel_21Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class InsUpdDel_21Response  implements java.io.Serializable {
    private boolean insUpdDel_21Result;

    public InsUpdDel_21Response() {
    }

    public InsUpdDel_21Response(
           boolean insUpdDel_21Result) {
           this.insUpdDel_21Result = insUpdDel_21Result;
    }


    /**
     * Gets the insUpdDel_21Result value for this InsUpdDel_21Response.
     * 
     * @return insUpdDel_21Result
     */
    public boolean isInsUpdDel_21Result() {
        return insUpdDel_21Result;
    }


    /**
     * Sets the insUpdDel_21Result value for this InsUpdDel_21Response.
     * 
     * @param insUpdDel_21Result
     */
    public void setInsUpdDel_21Result(boolean insUpdDel_21Result) {
        this.insUpdDel_21Result = insUpdDel_21Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InsUpdDel_21Response)) return false;
        InsUpdDel_21Response other = (InsUpdDel_21Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.insUpdDel_21Result == other.isInsUpdDel_21Result();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isInsUpdDel_21Result() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InsUpdDel_21Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">InsUpdDel_21Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insUpdDel_21Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "InsUpdDel_21Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
