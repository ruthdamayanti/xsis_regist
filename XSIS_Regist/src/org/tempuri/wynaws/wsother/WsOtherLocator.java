/**
 * WsOtherLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class WsOtherLocator extends org.apache.axis.client.Service implements org.tempuri.wynaws.wsother.WsOther {

    public WsOtherLocator() {
    }


    public WsOtherLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WsOtherLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WsOtherSoap
    private java.lang.String WsOtherSoap_address = "http://192.168.56.150/wynaws/wsother.asmx";

    public java.lang.String getWsOtherSoapAddress() {
        return WsOtherSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WsOtherSoapWSDDServiceName = "WsOtherSoap";

    public java.lang.String getWsOtherSoapWSDDServiceName() {
        return WsOtherSoapWSDDServiceName;
    }

    public void setWsOtherSoapWSDDServiceName(java.lang.String name) {
        WsOtherSoapWSDDServiceName = name;
    }

    public org.tempuri.wynaws.wsother.WsOtherSoap getWsOtherSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WsOtherSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWsOtherSoap(endpoint);
    }

    public org.tempuri.wynaws.wsother.WsOtherSoap getWsOtherSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.tempuri.wynaws.wsother.WsOtherSoapStub _stub = new org.tempuri.wynaws.wsother.WsOtherSoapStub(portAddress, this);
            _stub.setPortName(getWsOtherSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWsOtherSoapEndpointAddress(java.lang.String address) {
        WsOtherSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.tempuri.wynaws.wsother.WsOtherSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                org.tempuri.wynaws.wsother.WsOtherSoapStub _stub = new org.tempuri.wynaws.wsother.WsOtherSoapStub(new java.net.URL(WsOtherSoap_address), this);
                _stub.setPortName(getWsOtherSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WsOtherSoap".equals(inputPortName)) {
            return getWsOtherSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "WsOther");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "WsOtherSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WsOtherSoap".equals(portName)) {
            setWsOtherSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
