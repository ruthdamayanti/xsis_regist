/**
 * InsUpdDel_39.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class InsUpdDel_39  implements java.io.Serializable {
    private java.lang.String parameter1;

    private java.lang.String parameter2;

    private java.lang.String parameter3;

    private java.lang.String parameter4;

    private java.lang.String parameter5;

    private java.lang.String parameter6;

    private java.lang.String parameter7;

    private java.lang.String parameter8;

    private java.lang.String parameter9;

    private java.lang.String parameter10;

    private java.lang.String parameter11;

    private java.lang.String parameter12;

    private java.lang.String parameter13;

    private java.lang.String parameter14;

    private java.lang.String parameter15;

    private java.lang.String parameter16;

    private java.lang.String parameter17;

    private java.lang.String parameter18;

    private java.lang.String parameter19;

    private java.lang.String parameter20;

    private java.lang.String parameter21;

    private java.lang.String parameter22;

    private java.lang.String parameter23;

    private java.lang.String parameter24;

    private java.lang.String parameter25;

    private java.lang.String parameter26;

    private java.lang.String parameter27;

    private java.lang.String parameter28;

    private java.lang.String parameter29;

    private java.lang.String parameter30;

    private java.lang.String parameter31;

    private java.lang.String parameter32;

    private java.lang.String parameter33;

    private java.lang.String parameter34;

    private java.lang.String parameter35;

    private java.lang.String parameter36;

    private java.lang.String parameter37;

    private java.lang.String parameter38;

    private java.lang.String parameter39;

    private java.lang.String OUI;

    private java.lang.String OPW;

    private java.lang.String ODS;

    private java.lang.String oraPkg;

    public InsUpdDel_39() {
    }

    public InsUpdDel_39(
           java.lang.String parameter1,
           java.lang.String parameter2,
           java.lang.String parameter3,
           java.lang.String parameter4,
           java.lang.String parameter5,
           java.lang.String parameter6,
           java.lang.String parameter7,
           java.lang.String parameter8,
           java.lang.String parameter9,
           java.lang.String parameter10,
           java.lang.String parameter11,
           java.lang.String parameter12,
           java.lang.String parameter13,
           java.lang.String parameter14,
           java.lang.String parameter15,
           java.lang.String parameter16,
           java.lang.String parameter17,
           java.lang.String parameter18,
           java.lang.String parameter19,
           java.lang.String parameter20,
           java.lang.String parameter21,
           java.lang.String parameter22,
           java.lang.String parameter23,
           java.lang.String parameter24,
           java.lang.String parameter25,
           java.lang.String parameter26,
           java.lang.String parameter27,
           java.lang.String parameter28,
           java.lang.String parameter29,
           java.lang.String parameter30,
           java.lang.String parameter31,
           java.lang.String parameter32,
           java.lang.String parameter33,
           java.lang.String parameter34,
           java.lang.String parameter35,
           java.lang.String parameter36,
           java.lang.String parameter37,
           java.lang.String parameter38,
           java.lang.String parameter39,
           java.lang.String OUI,
           java.lang.String OPW,
           java.lang.String ODS,
           java.lang.String oraPkg) {
           this.parameter1 = parameter1;
           this.parameter2 = parameter2;
           this.parameter3 = parameter3;
           this.parameter4 = parameter4;
           this.parameter5 = parameter5;
           this.parameter6 = parameter6;
           this.parameter7 = parameter7;
           this.parameter8 = parameter8;
           this.parameter9 = parameter9;
           this.parameter10 = parameter10;
           this.parameter11 = parameter11;
           this.parameter12 = parameter12;
           this.parameter13 = parameter13;
           this.parameter14 = parameter14;
           this.parameter15 = parameter15;
           this.parameter16 = parameter16;
           this.parameter17 = parameter17;
           this.parameter18 = parameter18;
           this.parameter19 = parameter19;
           this.parameter20 = parameter20;
           this.parameter21 = parameter21;
           this.parameter22 = parameter22;
           this.parameter23 = parameter23;
           this.parameter24 = parameter24;
           this.parameter25 = parameter25;
           this.parameter26 = parameter26;
           this.parameter27 = parameter27;
           this.parameter28 = parameter28;
           this.parameter29 = parameter29;
           this.parameter30 = parameter30;
           this.parameter31 = parameter31;
           this.parameter32 = parameter32;
           this.parameter33 = parameter33;
           this.parameter34 = parameter34;
           this.parameter35 = parameter35;
           this.parameter36 = parameter36;
           this.parameter37 = parameter37;
           this.parameter38 = parameter38;
           this.parameter39 = parameter39;
           this.OUI = OUI;
           this.OPW = OPW;
           this.ODS = ODS;
           this.oraPkg = oraPkg;
    }


    /**
     * Gets the parameter1 value for this InsUpdDel_39.
     * 
     * @return parameter1
     */
    public java.lang.String getParameter1() {
        return parameter1;
    }


    /**
     * Sets the parameter1 value for this InsUpdDel_39.
     * 
     * @param parameter1
     */
    public void setParameter1(java.lang.String parameter1) {
        this.parameter1 = parameter1;
    }


    /**
     * Gets the parameter2 value for this InsUpdDel_39.
     * 
     * @return parameter2
     */
    public java.lang.String getParameter2() {
        return parameter2;
    }


    /**
     * Sets the parameter2 value for this InsUpdDel_39.
     * 
     * @param parameter2
     */
    public void setParameter2(java.lang.String parameter2) {
        this.parameter2 = parameter2;
    }


    /**
     * Gets the parameter3 value for this InsUpdDel_39.
     * 
     * @return parameter3
     */
    public java.lang.String getParameter3() {
        return parameter3;
    }


    /**
     * Sets the parameter3 value for this InsUpdDel_39.
     * 
     * @param parameter3
     */
    public void setParameter3(java.lang.String parameter3) {
        this.parameter3 = parameter3;
    }


    /**
     * Gets the parameter4 value for this InsUpdDel_39.
     * 
     * @return parameter4
     */
    public java.lang.String getParameter4() {
        return parameter4;
    }


    /**
     * Sets the parameter4 value for this InsUpdDel_39.
     * 
     * @param parameter4
     */
    public void setParameter4(java.lang.String parameter4) {
        this.parameter4 = parameter4;
    }


    /**
     * Gets the parameter5 value for this InsUpdDel_39.
     * 
     * @return parameter5
     */
    public java.lang.String getParameter5() {
        return parameter5;
    }


    /**
     * Sets the parameter5 value for this InsUpdDel_39.
     * 
     * @param parameter5
     */
    public void setParameter5(java.lang.String parameter5) {
        this.parameter5 = parameter5;
    }


    /**
     * Gets the parameter6 value for this InsUpdDel_39.
     * 
     * @return parameter6
     */
    public java.lang.String getParameter6() {
        return parameter6;
    }


    /**
     * Sets the parameter6 value for this InsUpdDel_39.
     * 
     * @param parameter6
     */
    public void setParameter6(java.lang.String parameter6) {
        this.parameter6 = parameter6;
    }


    /**
     * Gets the parameter7 value for this InsUpdDel_39.
     * 
     * @return parameter7
     */
    public java.lang.String getParameter7() {
        return parameter7;
    }


    /**
     * Sets the parameter7 value for this InsUpdDel_39.
     * 
     * @param parameter7
     */
    public void setParameter7(java.lang.String parameter7) {
        this.parameter7 = parameter7;
    }


    /**
     * Gets the parameter8 value for this InsUpdDel_39.
     * 
     * @return parameter8
     */
    public java.lang.String getParameter8() {
        return parameter8;
    }


    /**
     * Sets the parameter8 value for this InsUpdDel_39.
     * 
     * @param parameter8
     */
    public void setParameter8(java.lang.String parameter8) {
        this.parameter8 = parameter8;
    }


    /**
     * Gets the parameter9 value for this InsUpdDel_39.
     * 
     * @return parameter9
     */
    public java.lang.String getParameter9() {
        return parameter9;
    }


    /**
     * Sets the parameter9 value for this InsUpdDel_39.
     * 
     * @param parameter9
     */
    public void setParameter9(java.lang.String parameter9) {
        this.parameter9 = parameter9;
    }


    /**
     * Gets the parameter10 value for this InsUpdDel_39.
     * 
     * @return parameter10
     */
    public java.lang.String getParameter10() {
        return parameter10;
    }


    /**
     * Sets the parameter10 value for this InsUpdDel_39.
     * 
     * @param parameter10
     */
    public void setParameter10(java.lang.String parameter10) {
        this.parameter10 = parameter10;
    }


    /**
     * Gets the parameter11 value for this InsUpdDel_39.
     * 
     * @return parameter11
     */
    public java.lang.String getParameter11() {
        return parameter11;
    }


    /**
     * Sets the parameter11 value for this InsUpdDel_39.
     * 
     * @param parameter11
     */
    public void setParameter11(java.lang.String parameter11) {
        this.parameter11 = parameter11;
    }


    /**
     * Gets the parameter12 value for this InsUpdDel_39.
     * 
     * @return parameter12
     */
    public java.lang.String getParameter12() {
        return parameter12;
    }


    /**
     * Sets the parameter12 value for this InsUpdDel_39.
     * 
     * @param parameter12
     */
    public void setParameter12(java.lang.String parameter12) {
        this.parameter12 = parameter12;
    }


    /**
     * Gets the parameter13 value for this InsUpdDel_39.
     * 
     * @return parameter13
     */
    public java.lang.String getParameter13() {
        return parameter13;
    }


    /**
     * Sets the parameter13 value for this InsUpdDel_39.
     * 
     * @param parameter13
     */
    public void setParameter13(java.lang.String parameter13) {
        this.parameter13 = parameter13;
    }


    /**
     * Gets the parameter14 value for this InsUpdDel_39.
     * 
     * @return parameter14
     */
    public java.lang.String getParameter14() {
        return parameter14;
    }


    /**
     * Sets the parameter14 value for this InsUpdDel_39.
     * 
     * @param parameter14
     */
    public void setParameter14(java.lang.String parameter14) {
        this.parameter14 = parameter14;
    }


    /**
     * Gets the parameter15 value for this InsUpdDel_39.
     * 
     * @return parameter15
     */
    public java.lang.String getParameter15() {
        return parameter15;
    }


    /**
     * Sets the parameter15 value for this InsUpdDel_39.
     * 
     * @param parameter15
     */
    public void setParameter15(java.lang.String parameter15) {
        this.parameter15 = parameter15;
    }


    /**
     * Gets the parameter16 value for this InsUpdDel_39.
     * 
     * @return parameter16
     */
    public java.lang.String getParameter16() {
        return parameter16;
    }


    /**
     * Sets the parameter16 value for this InsUpdDel_39.
     * 
     * @param parameter16
     */
    public void setParameter16(java.lang.String parameter16) {
        this.parameter16 = parameter16;
    }


    /**
     * Gets the parameter17 value for this InsUpdDel_39.
     * 
     * @return parameter17
     */
    public java.lang.String getParameter17() {
        return parameter17;
    }


    /**
     * Sets the parameter17 value for this InsUpdDel_39.
     * 
     * @param parameter17
     */
    public void setParameter17(java.lang.String parameter17) {
        this.parameter17 = parameter17;
    }


    /**
     * Gets the parameter18 value for this InsUpdDel_39.
     * 
     * @return parameter18
     */
    public java.lang.String getParameter18() {
        return parameter18;
    }


    /**
     * Sets the parameter18 value for this InsUpdDel_39.
     * 
     * @param parameter18
     */
    public void setParameter18(java.lang.String parameter18) {
        this.parameter18 = parameter18;
    }


    /**
     * Gets the parameter19 value for this InsUpdDel_39.
     * 
     * @return parameter19
     */
    public java.lang.String getParameter19() {
        return parameter19;
    }


    /**
     * Sets the parameter19 value for this InsUpdDel_39.
     * 
     * @param parameter19
     */
    public void setParameter19(java.lang.String parameter19) {
        this.parameter19 = parameter19;
    }


    /**
     * Gets the parameter20 value for this InsUpdDel_39.
     * 
     * @return parameter20
     */
    public java.lang.String getParameter20() {
        return parameter20;
    }


    /**
     * Sets the parameter20 value for this InsUpdDel_39.
     * 
     * @param parameter20
     */
    public void setParameter20(java.lang.String parameter20) {
        this.parameter20 = parameter20;
    }


    /**
     * Gets the parameter21 value for this InsUpdDel_39.
     * 
     * @return parameter21
     */
    public java.lang.String getParameter21() {
        return parameter21;
    }


    /**
     * Sets the parameter21 value for this InsUpdDel_39.
     * 
     * @param parameter21
     */
    public void setParameter21(java.lang.String parameter21) {
        this.parameter21 = parameter21;
    }


    /**
     * Gets the parameter22 value for this InsUpdDel_39.
     * 
     * @return parameter22
     */
    public java.lang.String getParameter22() {
        return parameter22;
    }


    /**
     * Sets the parameter22 value for this InsUpdDel_39.
     * 
     * @param parameter22
     */
    public void setParameter22(java.lang.String parameter22) {
        this.parameter22 = parameter22;
    }


    /**
     * Gets the parameter23 value for this InsUpdDel_39.
     * 
     * @return parameter23
     */
    public java.lang.String getParameter23() {
        return parameter23;
    }


    /**
     * Sets the parameter23 value for this InsUpdDel_39.
     * 
     * @param parameter23
     */
    public void setParameter23(java.lang.String parameter23) {
        this.parameter23 = parameter23;
    }


    /**
     * Gets the parameter24 value for this InsUpdDel_39.
     * 
     * @return parameter24
     */
    public java.lang.String getParameter24() {
        return parameter24;
    }


    /**
     * Sets the parameter24 value for this InsUpdDel_39.
     * 
     * @param parameter24
     */
    public void setParameter24(java.lang.String parameter24) {
        this.parameter24 = parameter24;
    }


    /**
     * Gets the parameter25 value for this InsUpdDel_39.
     * 
     * @return parameter25
     */
    public java.lang.String getParameter25() {
        return parameter25;
    }


    /**
     * Sets the parameter25 value for this InsUpdDel_39.
     * 
     * @param parameter25
     */
    public void setParameter25(java.lang.String parameter25) {
        this.parameter25 = parameter25;
    }


    /**
     * Gets the parameter26 value for this InsUpdDel_39.
     * 
     * @return parameter26
     */
    public java.lang.String getParameter26() {
        return parameter26;
    }


    /**
     * Sets the parameter26 value for this InsUpdDel_39.
     * 
     * @param parameter26
     */
    public void setParameter26(java.lang.String parameter26) {
        this.parameter26 = parameter26;
    }


    /**
     * Gets the parameter27 value for this InsUpdDel_39.
     * 
     * @return parameter27
     */
    public java.lang.String getParameter27() {
        return parameter27;
    }


    /**
     * Sets the parameter27 value for this InsUpdDel_39.
     * 
     * @param parameter27
     */
    public void setParameter27(java.lang.String parameter27) {
        this.parameter27 = parameter27;
    }


    /**
     * Gets the parameter28 value for this InsUpdDel_39.
     * 
     * @return parameter28
     */
    public java.lang.String getParameter28() {
        return parameter28;
    }


    /**
     * Sets the parameter28 value for this InsUpdDel_39.
     * 
     * @param parameter28
     */
    public void setParameter28(java.lang.String parameter28) {
        this.parameter28 = parameter28;
    }


    /**
     * Gets the parameter29 value for this InsUpdDel_39.
     * 
     * @return parameter29
     */
    public java.lang.String getParameter29() {
        return parameter29;
    }


    /**
     * Sets the parameter29 value for this InsUpdDel_39.
     * 
     * @param parameter29
     */
    public void setParameter29(java.lang.String parameter29) {
        this.parameter29 = parameter29;
    }


    /**
     * Gets the parameter30 value for this InsUpdDel_39.
     * 
     * @return parameter30
     */
    public java.lang.String getParameter30() {
        return parameter30;
    }


    /**
     * Sets the parameter30 value for this InsUpdDel_39.
     * 
     * @param parameter30
     */
    public void setParameter30(java.lang.String parameter30) {
        this.parameter30 = parameter30;
    }


    /**
     * Gets the parameter31 value for this InsUpdDel_39.
     * 
     * @return parameter31
     */
    public java.lang.String getParameter31() {
        return parameter31;
    }


    /**
     * Sets the parameter31 value for this InsUpdDel_39.
     * 
     * @param parameter31
     */
    public void setParameter31(java.lang.String parameter31) {
        this.parameter31 = parameter31;
    }


    /**
     * Gets the parameter32 value for this InsUpdDel_39.
     * 
     * @return parameter32
     */
    public java.lang.String getParameter32() {
        return parameter32;
    }


    /**
     * Sets the parameter32 value for this InsUpdDel_39.
     * 
     * @param parameter32
     */
    public void setParameter32(java.lang.String parameter32) {
        this.parameter32 = parameter32;
    }


    /**
     * Gets the parameter33 value for this InsUpdDel_39.
     * 
     * @return parameter33
     */
    public java.lang.String getParameter33() {
        return parameter33;
    }


    /**
     * Sets the parameter33 value for this InsUpdDel_39.
     * 
     * @param parameter33
     */
    public void setParameter33(java.lang.String parameter33) {
        this.parameter33 = parameter33;
    }


    /**
     * Gets the parameter34 value for this InsUpdDel_39.
     * 
     * @return parameter34
     */
    public java.lang.String getParameter34() {
        return parameter34;
    }


    /**
     * Sets the parameter34 value for this InsUpdDel_39.
     * 
     * @param parameter34
     */
    public void setParameter34(java.lang.String parameter34) {
        this.parameter34 = parameter34;
    }


    /**
     * Gets the parameter35 value for this InsUpdDel_39.
     * 
     * @return parameter35
     */
    public java.lang.String getParameter35() {
        return parameter35;
    }


    /**
     * Sets the parameter35 value for this InsUpdDel_39.
     * 
     * @param parameter35
     */
    public void setParameter35(java.lang.String parameter35) {
        this.parameter35 = parameter35;
    }


    /**
     * Gets the parameter36 value for this InsUpdDel_39.
     * 
     * @return parameter36
     */
    public java.lang.String getParameter36() {
        return parameter36;
    }


    /**
     * Sets the parameter36 value for this InsUpdDel_39.
     * 
     * @param parameter36
     */
    public void setParameter36(java.lang.String parameter36) {
        this.parameter36 = parameter36;
    }


    /**
     * Gets the parameter37 value for this InsUpdDel_39.
     * 
     * @return parameter37
     */
    public java.lang.String getParameter37() {
        return parameter37;
    }


    /**
     * Sets the parameter37 value for this InsUpdDel_39.
     * 
     * @param parameter37
     */
    public void setParameter37(java.lang.String parameter37) {
        this.parameter37 = parameter37;
    }


    /**
     * Gets the parameter38 value for this InsUpdDel_39.
     * 
     * @return parameter38
     */
    public java.lang.String getParameter38() {
        return parameter38;
    }


    /**
     * Sets the parameter38 value for this InsUpdDel_39.
     * 
     * @param parameter38
     */
    public void setParameter38(java.lang.String parameter38) {
        this.parameter38 = parameter38;
    }


    /**
     * Gets the parameter39 value for this InsUpdDel_39.
     * 
     * @return parameter39
     */
    public java.lang.String getParameter39() {
        return parameter39;
    }


    /**
     * Sets the parameter39 value for this InsUpdDel_39.
     * 
     * @param parameter39
     */
    public void setParameter39(java.lang.String parameter39) {
        this.parameter39 = parameter39;
    }


    /**
     * Gets the OUI value for this InsUpdDel_39.
     * 
     * @return OUI
     */
    public java.lang.String getOUI() {
        return OUI;
    }


    /**
     * Sets the OUI value for this InsUpdDel_39.
     * 
     * @param OUI
     */
    public void setOUI(java.lang.String OUI) {
        this.OUI = OUI;
    }


    /**
     * Gets the OPW value for this InsUpdDel_39.
     * 
     * @return OPW
     */
    public java.lang.String getOPW() {
        return OPW;
    }


    /**
     * Sets the OPW value for this InsUpdDel_39.
     * 
     * @param OPW
     */
    public void setOPW(java.lang.String OPW) {
        this.OPW = OPW;
    }


    /**
     * Gets the ODS value for this InsUpdDel_39.
     * 
     * @return ODS
     */
    public java.lang.String getODS() {
        return ODS;
    }


    /**
     * Sets the ODS value for this InsUpdDel_39.
     * 
     * @param ODS
     */
    public void setODS(java.lang.String ODS) {
        this.ODS = ODS;
    }


    /**
     * Gets the oraPkg value for this InsUpdDel_39.
     * 
     * @return oraPkg
     */
    public java.lang.String getOraPkg() {
        return oraPkg;
    }


    /**
     * Sets the oraPkg value for this InsUpdDel_39.
     * 
     * @param oraPkg
     */
    public void setOraPkg(java.lang.String oraPkg) {
        this.oraPkg = oraPkg;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InsUpdDel_39)) return false;
        InsUpdDel_39 other = (InsUpdDel_39) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.parameter1==null && other.getParameter1()==null) || 
             (this.parameter1!=null &&
              this.parameter1.equals(other.getParameter1()))) &&
            ((this.parameter2==null && other.getParameter2()==null) || 
             (this.parameter2!=null &&
              this.parameter2.equals(other.getParameter2()))) &&
            ((this.parameter3==null && other.getParameter3()==null) || 
             (this.parameter3!=null &&
              this.parameter3.equals(other.getParameter3()))) &&
            ((this.parameter4==null && other.getParameter4()==null) || 
             (this.parameter4!=null &&
              this.parameter4.equals(other.getParameter4()))) &&
            ((this.parameter5==null && other.getParameter5()==null) || 
             (this.parameter5!=null &&
              this.parameter5.equals(other.getParameter5()))) &&
            ((this.parameter6==null && other.getParameter6()==null) || 
             (this.parameter6!=null &&
              this.parameter6.equals(other.getParameter6()))) &&
            ((this.parameter7==null && other.getParameter7()==null) || 
             (this.parameter7!=null &&
              this.parameter7.equals(other.getParameter7()))) &&
            ((this.parameter8==null && other.getParameter8()==null) || 
             (this.parameter8!=null &&
              this.parameter8.equals(other.getParameter8()))) &&
            ((this.parameter9==null && other.getParameter9()==null) || 
             (this.parameter9!=null &&
              this.parameter9.equals(other.getParameter9()))) &&
            ((this.parameter10==null && other.getParameter10()==null) || 
             (this.parameter10!=null &&
              this.parameter10.equals(other.getParameter10()))) &&
            ((this.parameter11==null && other.getParameter11()==null) || 
             (this.parameter11!=null &&
              this.parameter11.equals(other.getParameter11()))) &&
            ((this.parameter12==null && other.getParameter12()==null) || 
             (this.parameter12!=null &&
              this.parameter12.equals(other.getParameter12()))) &&
            ((this.parameter13==null && other.getParameter13()==null) || 
             (this.parameter13!=null &&
              this.parameter13.equals(other.getParameter13()))) &&
            ((this.parameter14==null && other.getParameter14()==null) || 
             (this.parameter14!=null &&
              this.parameter14.equals(other.getParameter14()))) &&
            ((this.parameter15==null && other.getParameter15()==null) || 
             (this.parameter15!=null &&
              this.parameter15.equals(other.getParameter15()))) &&
            ((this.parameter16==null && other.getParameter16()==null) || 
             (this.parameter16!=null &&
              this.parameter16.equals(other.getParameter16()))) &&
            ((this.parameter17==null && other.getParameter17()==null) || 
             (this.parameter17!=null &&
              this.parameter17.equals(other.getParameter17()))) &&
            ((this.parameter18==null && other.getParameter18()==null) || 
             (this.parameter18!=null &&
              this.parameter18.equals(other.getParameter18()))) &&
            ((this.parameter19==null && other.getParameter19()==null) || 
             (this.parameter19!=null &&
              this.parameter19.equals(other.getParameter19()))) &&
            ((this.parameter20==null && other.getParameter20()==null) || 
             (this.parameter20!=null &&
              this.parameter20.equals(other.getParameter20()))) &&
            ((this.parameter21==null && other.getParameter21()==null) || 
             (this.parameter21!=null &&
              this.parameter21.equals(other.getParameter21()))) &&
            ((this.parameter22==null && other.getParameter22()==null) || 
             (this.parameter22!=null &&
              this.parameter22.equals(other.getParameter22()))) &&
            ((this.parameter23==null && other.getParameter23()==null) || 
             (this.parameter23!=null &&
              this.parameter23.equals(other.getParameter23()))) &&
            ((this.parameter24==null && other.getParameter24()==null) || 
             (this.parameter24!=null &&
              this.parameter24.equals(other.getParameter24()))) &&
            ((this.parameter25==null && other.getParameter25()==null) || 
             (this.parameter25!=null &&
              this.parameter25.equals(other.getParameter25()))) &&
            ((this.parameter26==null && other.getParameter26()==null) || 
             (this.parameter26!=null &&
              this.parameter26.equals(other.getParameter26()))) &&
            ((this.parameter27==null && other.getParameter27()==null) || 
             (this.parameter27!=null &&
              this.parameter27.equals(other.getParameter27()))) &&
            ((this.parameter28==null && other.getParameter28()==null) || 
             (this.parameter28!=null &&
              this.parameter28.equals(other.getParameter28()))) &&
            ((this.parameter29==null && other.getParameter29()==null) || 
             (this.parameter29!=null &&
              this.parameter29.equals(other.getParameter29()))) &&
            ((this.parameter30==null && other.getParameter30()==null) || 
             (this.parameter30!=null &&
              this.parameter30.equals(other.getParameter30()))) &&
            ((this.parameter31==null && other.getParameter31()==null) || 
             (this.parameter31!=null &&
              this.parameter31.equals(other.getParameter31()))) &&
            ((this.parameter32==null && other.getParameter32()==null) || 
             (this.parameter32!=null &&
              this.parameter32.equals(other.getParameter32()))) &&
            ((this.parameter33==null && other.getParameter33()==null) || 
             (this.parameter33!=null &&
              this.parameter33.equals(other.getParameter33()))) &&
            ((this.parameter34==null && other.getParameter34()==null) || 
             (this.parameter34!=null &&
              this.parameter34.equals(other.getParameter34()))) &&
            ((this.parameter35==null && other.getParameter35()==null) || 
             (this.parameter35!=null &&
              this.parameter35.equals(other.getParameter35()))) &&
            ((this.parameter36==null && other.getParameter36()==null) || 
             (this.parameter36!=null &&
              this.parameter36.equals(other.getParameter36()))) &&
            ((this.parameter37==null && other.getParameter37()==null) || 
             (this.parameter37!=null &&
              this.parameter37.equals(other.getParameter37()))) &&
            ((this.parameter38==null && other.getParameter38()==null) || 
             (this.parameter38!=null &&
              this.parameter38.equals(other.getParameter38()))) &&
            ((this.parameter39==null && other.getParameter39()==null) || 
             (this.parameter39!=null &&
              this.parameter39.equals(other.getParameter39()))) &&
            ((this.OUI==null && other.getOUI()==null) || 
             (this.OUI!=null &&
              this.OUI.equals(other.getOUI()))) &&
            ((this.OPW==null && other.getOPW()==null) || 
             (this.OPW!=null &&
              this.OPW.equals(other.getOPW()))) &&
            ((this.ODS==null && other.getODS()==null) || 
             (this.ODS!=null &&
              this.ODS.equals(other.getODS()))) &&
            ((this.oraPkg==null && other.getOraPkg()==null) || 
             (this.oraPkg!=null &&
              this.oraPkg.equals(other.getOraPkg())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getParameter1() != null) {
            _hashCode += getParameter1().hashCode();
        }
        if (getParameter2() != null) {
            _hashCode += getParameter2().hashCode();
        }
        if (getParameter3() != null) {
            _hashCode += getParameter3().hashCode();
        }
        if (getParameter4() != null) {
            _hashCode += getParameter4().hashCode();
        }
        if (getParameter5() != null) {
            _hashCode += getParameter5().hashCode();
        }
        if (getParameter6() != null) {
            _hashCode += getParameter6().hashCode();
        }
        if (getParameter7() != null) {
            _hashCode += getParameter7().hashCode();
        }
        if (getParameter8() != null) {
            _hashCode += getParameter8().hashCode();
        }
        if (getParameter9() != null) {
            _hashCode += getParameter9().hashCode();
        }
        if (getParameter10() != null) {
            _hashCode += getParameter10().hashCode();
        }
        if (getParameter11() != null) {
            _hashCode += getParameter11().hashCode();
        }
        if (getParameter12() != null) {
            _hashCode += getParameter12().hashCode();
        }
        if (getParameter13() != null) {
            _hashCode += getParameter13().hashCode();
        }
        if (getParameter14() != null) {
            _hashCode += getParameter14().hashCode();
        }
        if (getParameter15() != null) {
            _hashCode += getParameter15().hashCode();
        }
        if (getParameter16() != null) {
            _hashCode += getParameter16().hashCode();
        }
        if (getParameter17() != null) {
            _hashCode += getParameter17().hashCode();
        }
        if (getParameter18() != null) {
            _hashCode += getParameter18().hashCode();
        }
        if (getParameter19() != null) {
            _hashCode += getParameter19().hashCode();
        }
        if (getParameter20() != null) {
            _hashCode += getParameter20().hashCode();
        }
        if (getParameter21() != null) {
            _hashCode += getParameter21().hashCode();
        }
        if (getParameter22() != null) {
            _hashCode += getParameter22().hashCode();
        }
        if (getParameter23() != null) {
            _hashCode += getParameter23().hashCode();
        }
        if (getParameter24() != null) {
            _hashCode += getParameter24().hashCode();
        }
        if (getParameter25() != null) {
            _hashCode += getParameter25().hashCode();
        }
        if (getParameter26() != null) {
            _hashCode += getParameter26().hashCode();
        }
        if (getParameter27() != null) {
            _hashCode += getParameter27().hashCode();
        }
        if (getParameter28() != null) {
            _hashCode += getParameter28().hashCode();
        }
        if (getParameter29() != null) {
            _hashCode += getParameter29().hashCode();
        }
        if (getParameter30() != null) {
            _hashCode += getParameter30().hashCode();
        }
        if (getParameter31() != null) {
            _hashCode += getParameter31().hashCode();
        }
        if (getParameter32() != null) {
            _hashCode += getParameter32().hashCode();
        }
        if (getParameter33() != null) {
            _hashCode += getParameter33().hashCode();
        }
        if (getParameter34() != null) {
            _hashCode += getParameter34().hashCode();
        }
        if (getParameter35() != null) {
            _hashCode += getParameter35().hashCode();
        }
        if (getParameter36() != null) {
            _hashCode += getParameter36().hashCode();
        }
        if (getParameter37() != null) {
            _hashCode += getParameter37().hashCode();
        }
        if (getParameter38() != null) {
            _hashCode += getParameter38().hashCode();
        }
        if (getParameter39() != null) {
            _hashCode += getParameter39().hashCode();
        }
        if (getOUI() != null) {
            _hashCode += getOUI().hashCode();
        }
        if (getOPW() != null) {
            _hashCode += getOPW().hashCode();
        }
        if (getODS() != null) {
            _hashCode += getODS().hashCode();
        }
        if (getOraPkg() != null) {
            _hashCode += getOraPkg().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InsUpdDel_39.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">InsUpdDel_39"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter5");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter6");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter6"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter7");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter7"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter8");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter8"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter9");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter9"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter10");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter10"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter11");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter11"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter12");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter12"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter13");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter13"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter14");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter14"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter15");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter15"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter16");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter16"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter17");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter17"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter18");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter18"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter19");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter19"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter20");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter20"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter21");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter21"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter22");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter22"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter23");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter23"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter24");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter24"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter25");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter25"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter26");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter26"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter27");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter27"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter28");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter28"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter29");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter29"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter30");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter30"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter31");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter31"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter32");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter32"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter33");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter33"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter34");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter34"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter35");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter35"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter36");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter36"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter37");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter37"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter38");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter38"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter39");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "parameter39"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OUI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OUI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OPW");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OPW"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ODS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "ODS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oraPkg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OraPkg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
