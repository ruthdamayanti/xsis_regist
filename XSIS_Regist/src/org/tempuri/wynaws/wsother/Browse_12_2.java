/**
 * Browse_12_2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_12_2  implements java.io.Serializable {
    private java.lang.String parameter1;

    private java.lang.String parameter2;

    private java.lang.String OUI;

    private java.lang.String OPW;

    private java.lang.String ODS;

    private java.lang.String oraPkg;

    public Browse_12_2() {
    }

    public Browse_12_2(
           java.lang.String parameter1,
           java.lang.String parameter2,
           java.lang.String OUI,
           java.lang.String OPW,
           java.lang.String ODS,
           java.lang.String oraPkg) {
           this.parameter1 = parameter1;
           this.parameter2 = parameter2;
           this.OUI = OUI;
           this.OPW = OPW;
           this.ODS = ODS;
           this.oraPkg = oraPkg;
    }


    /**
     * Gets the parameter1 value for this Browse_12_2.
     * 
     * @return parameter1
     */
    public java.lang.String getParameter1() {
        return parameter1;
    }


    /**
     * Sets the parameter1 value for this Browse_12_2.
     * 
     * @param parameter1
     */
    public void setParameter1(java.lang.String parameter1) {
        this.parameter1 = parameter1;
    }


    /**
     * Gets the parameter2 value for this Browse_12_2.
     * 
     * @return parameter2
     */
    public java.lang.String getParameter2() {
        return parameter2;
    }


    /**
     * Sets the parameter2 value for this Browse_12_2.
     * 
     * @param parameter2
     */
    public void setParameter2(java.lang.String parameter2) {
        this.parameter2 = parameter2;
    }


    /**
     * Gets the OUI value for this Browse_12_2.
     * 
     * @return OUI
     */
    public java.lang.String getOUI() {
        return OUI;
    }


    /**
     * Sets the OUI value for this Browse_12_2.
     * 
     * @param OUI
     */
    public void setOUI(java.lang.String OUI) {
        this.OUI = OUI;
    }


    /**
     * Gets the OPW value for this Browse_12_2.
     * 
     * @return OPW
     */
    public java.lang.String getOPW() {
        return OPW;
    }


    /**
     * Sets the OPW value for this Browse_12_2.
     * 
     * @param OPW
     */
    public void setOPW(java.lang.String OPW) {
        this.OPW = OPW;
    }


    /**
     * Gets the ODS value for this Browse_12_2.
     * 
     * @return ODS
     */
    public java.lang.String getODS() {
        return ODS;
    }


    /**
     * Sets the ODS value for this Browse_12_2.
     * 
     * @param ODS
     */
    public void setODS(java.lang.String ODS) {
        this.ODS = ODS;
    }


    /**
     * Gets the oraPkg value for this Browse_12_2.
     * 
     * @return oraPkg
     */
    public java.lang.String getOraPkg() {
        return oraPkg;
    }


    /**
     * Sets the oraPkg value for this Browse_12_2.
     * 
     * @param oraPkg
     */
    public void setOraPkg(java.lang.String oraPkg) {
        this.oraPkg = oraPkg;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_12_2)) return false;
        Browse_12_2 other = (Browse_12_2) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.parameter1==null && other.getParameter1()==null) || 
             (this.parameter1!=null &&
              this.parameter1.equals(other.getParameter1()))) &&
            ((this.parameter2==null && other.getParameter2()==null) || 
             (this.parameter2!=null &&
              this.parameter2.equals(other.getParameter2()))) &&
            ((this.OUI==null && other.getOUI()==null) || 
             (this.OUI!=null &&
              this.OUI.equals(other.getOUI()))) &&
            ((this.OPW==null && other.getOPW()==null) || 
             (this.OPW!=null &&
              this.OPW.equals(other.getOPW()))) &&
            ((this.ODS==null && other.getODS()==null) || 
             (this.ODS!=null &&
              this.ODS.equals(other.getODS()))) &&
            ((this.oraPkg==null && other.getOraPkg()==null) || 
             (this.oraPkg!=null &&
              this.oraPkg.equals(other.getOraPkg())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getParameter1() != null) {
            _hashCode += getParameter1().hashCode();
        }
        if (getParameter2() != null) {
            _hashCode += getParameter2().hashCode();
        }
        if (getOUI() != null) {
            _hashCode += getOUI().hashCode();
        }
        if (getOPW() != null) {
            _hashCode += getOPW().hashCode();
        }
        if (getODS() != null) {
            _hashCode += getODS().hashCode();
        }
        if (getOraPkg() != null) {
            _hashCode += getOraPkg().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_12_2.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_12_2"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Parameter1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Parameter2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OUI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OUI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OPW");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OPW"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ODS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "ODS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oraPkg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OraPkg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
