/**
 * Browse_1_15Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_1_15Response  implements java.io.Serializable {
    private org.tempuri.wynaws.wsother.Browse_1_15ResponseBrowse_1_15Result browse_1_15Result;

    public Browse_1_15Response() {
    }

    public Browse_1_15Response(
           org.tempuri.wynaws.wsother.Browse_1_15ResponseBrowse_1_15Result browse_1_15Result) {
           this.browse_1_15Result = browse_1_15Result;
    }


    /**
     * Gets the browse_1_15Result value for this Browse_1_15Response.
     * 
     * @return browse_1_15Result
     */
    public org.tempuri.wynaws.wsother.Browse_1_15ResponseBrowse_1_15Result getBrowse_1_15Result() {
        return browse_1_15Result;
    }


    /**
     * Sets the browse_1_15Result value for this Browse_1_15Response.
     * 
     * @param browse_1_15Result
     */
    public void setBrowse_1_15Result(org.tempuri.wynaws.wsother.Browse_1_15ResponseBrowse_1_15Result browse_1_15Result) {
        this.browse_1_15Result = browse_1_15Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_1_15Response)) return false;
        Browse_1_15Response other = (Browse_1_15Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browse_1_15Result==null && other.getBrowse_1_15Result()==null) || 
             (this.browse_1_15Result!=null &&
              this.browse_1_15Result.equals(other.getBrowse_1_15Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowse_1_15Result() != null) {
            _hashCode += getBrowse_1_15Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_1_15Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_1_15Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browse_1_15Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Browse_1_15Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">>Browse_1_15Response>Browse_1_15Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
