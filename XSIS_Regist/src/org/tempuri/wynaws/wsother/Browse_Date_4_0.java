/**
 * Browse_Date_4_0.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_Date_4_0  implements java.io.Serializable {
    private java.util.Calendar DATE1;

    private java.util.Calendar DATE2;

    private java.lang.String OUI;

    private java.lang.String OPW;

    private java.lang.String ODS;

    private java.lang.String oraPkg;

    public Browse_Date_4_0() {
    }

    public Browse_Date_4_0(
           java.util.Calendar DATE1,
           java.util.Calendar DATE2,
           java.lang.String OUI,
           java.lang.String OPW,
           java.lang.String ODS,
           java.lang.String oraPkg) {
           this.DATE1 = DATE1;
           this.DATE2 = DATE2;
           this.OUI = OUI;
           this.OPW = OPW;
           this.ODS = ODS;
           this.oraPkg = oraPkg;
    }


    /**
     * Gets the DATE1 value for this Browse_Date_4_0.
     * 
     * @return DATE1
     */
    public java.util.Calendar getDATE1() {
        return DATE1;
    }


    /**
     * Sets the DATE1 value for this Browse_Date_4_0.
     * 
     * @param DATE1
     */
    public void setDATE1(java.util.Calendar DATE1) {
        this.DATE1 = DATE1;
    }


    /**
     * Gets the DATE2 value for this Browse_Date_4_0.
     * 
     * @return DATE2
     */
    public java.util.Calendar getDATE2() {
        return DATE2;
    }


    /**
     * Sets the DATE2 value for this Browse_Date_4_0.
     * 
     * @param DATE2
     */
    public void setDATE2(java.util.Calendar DATE2) {
        this.DATE2 = DATE2;
    }


    /**
     * Gets the OUI value for this Browse_Date_4_0.
     * 
     * @return OUI
     */
    public java.lang.String getOUI() {
        return OUI;
    }


    /**
     * Sets the OUI value for this Browse_Date_4_0.
     * 
     * @param OUI
     */
    public void setOUI(java.lang.String OUI) {
        this.OUI = OUI;
    }


    /**
     * Gets the OPW value for this Browse_Date_4_0.
     * 
     * @return OPW
     */
    public java.lang.String getOPW() {
        return OPW;
    }


    /**
     * Sets the OPW value for this Browse_Date_4_0.
     * 
     * @param OPW
     */
    public void setOPW(java.lang.String OPW) {
        this.OPW = OPW;
    }


    /**
     * Gets the ODS value for this Browse_Date_4_0.
     * 
     * @return ODS
     */
    public java.lang.String getODS() {
        return ODS;
    }


    /**
     * Sets the ODS value for this Browse_Date_4_0.
     * 
     * @param ODS
     */
    public void setODS(java.lang.String ODS) {
        this.ODS = ODS;
    }


    /**
     * Gets the oraPkg value for this Browse_Date_4_0.
     * 
     * @return oraPkg
     */
    public java.lang.String getOraPkg() {
        return oraPkg;
    }


    /**
     * Sets the oraPkg value for this Browse_Date_4_0.
     * 
     * @param oraPkg
     */
    public void setOraPkg(java.lang.String oraPkg) {
        this.oraPkg = oraPkg;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_Date_4_0)) return false;
        Browse_Date_4_0 other = (Browse_Date_4_0) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DATE1==null && other.getDATE1()==null) || 
             (this.DATE1!=null &&
              this.DATE1.equals(other.getDATE1()))) &&
            ((this.DATE2==null && other.getDATE2()==null) || 
             (this.DATE2!=null &&
              this.DATE2.equals(other.getDATE2()))) &&
            ((this.OUI==null && other.getOUI()==null) || 
             (this.OUI!=null &&
              this.OUI.equals(other.getOUI()))) &&
            ((this.OPW==null && other.getOPW()==null) || 
             (this.OPW!=null &&
              this.OPW.equals(other.getOPW()))) &&
            ((this.ODS==null && other.getODS()==null) || 
             (this.ODS!=null &&
              this.ODS.equals(other.getODS()))) &&
            ((this.oraPkg==null && other.getOraPkg()==null) || 
             (this.oraPkg!=null &&
              this.oraPkg.equals(other.getOraPkg())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDATE1() != null) {
            _hashCode += getDATE1().hashCode();
        }
        if (getDATE2() != null) {
            _hashCode += getDATE2().hashCode();
        }
        if (getOUI() != null) {
            _hashCode += getOUI().hashCode();
        }
        if (getOPW() != null) {
            _hashCode += getOPW().hashCode();
        }
        if (getODS() != null) {
            _hashCode += getODS().hashCode();
        }
        if (getOraPkg() != null) {
            _hashCode += getOraPkg().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_Date_4_0.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_Date_4_0"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATE1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "DATE1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATE2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "DATE2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OUI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OUI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OPW");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OPW"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ODS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "ODS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oraPkg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "OraPkg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
