/**
 * Browse_Date_3_3Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.wynaws.wsother;

public class Browse_Date_3_3Response  implements java.io.Serializable {
    private org.tempuri.wynaws.wsother.Browse_Date_3_3ResponseBrowse_Date_3_3Result browse_Date_3_3Result;

    public Browse_Date_3_3Response() {
    }

    public Browse_Date_3_3Response(
           org.tempuri.wynaws.wsother.Browse_Date_3_3ResponseBrowse_Date_3_3Result browse_Date_3_3Result) {
           this.browse_Date_3_3Result = browse_Date_3_3Result;
    }


    /**
     * Gets the browse_Date_3_3Result value for this Browse_Date_3_3Response.
     * 
     * @return browse_Date_3_3Result
     */
    public org.tempuri.wynaws.wsother.Browse_Date_3_3ResponseBrowse_Date_3_3Result getBrowse_Date_3_3Result() {
        return browse_Date_3_3Result;
    }


    /**
     * Sets the browse_Date_3_3Result value for this Browse_Date_3_3Response.
     * 
     * @param browse_Date_3_3Result
     */
    public void setBrowse_Date_3_3Result(org.tempuri.wynaws.wsother.Browse_Date_3_3ResponseBrowse_Date_3_3Result browse_Date_3_3Result) {
        this.browse_Date_3_3Result = browse_Date_3_3Result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Browse_Date_3_3Response)) return false;
        Browse_Date_3_3Response other = (Browse_Date_3_3Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browse_Date_3_3Result==null && other.getBrowse_Date_3_3Result()==null) || 
             (this.browse_Date_3_3Result!=null &&
              this.browse_Date_3_3Result.equals(other.getBrowse_Date_3_3Result())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowse_Date_3_3Result() != null) {
            _hashCode += getBrowse_Date_3_3Result().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Browse_Date_3_3Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">Browse_Date_3_3Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browse_Date_3_3Result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", "Browse_Date_3_3Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/wynaws/wsother", ">>Browse_Date_3_3Response>Browse_Date_3_3Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
