<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>XSIS Registration by Patient</title>
    <!-- telerik files -->
<link href="styles/kendo.common-bootstrap.min.css" rel="stylesheet">
<link href="styles/kendo.default.min.css" rel="stylesheet">
<!-- Data Table -->
<link href="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" rel="stylesheet">
<!-- jquery and kendo javascript. jquery MUST be first. -->
<script src="js/jquery.min.js"></script>
<script src="js/kendo.all.min.js"></script>
	<link rel="icon" href="image/prodiafavicon.ico" type="image/gif" sizes="16x16">
</head>
<body><jsp:include page="header.jsp"/><br>
         <div class="container-fluid text-center">
            <div class="row content">
                <div class="col-sm-9 text-left"> 
                    <div class="card">
                        <div class="card-header">
                            Form Registrasi Pasien
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"></h5>
                            <form method="post" action="requisition.jsp">
                            <div class="form-group row">
                                <label for="referee" class="col-sm-2 col-form-label">Referee</label>
                                <div class="col-sm-10">
                                   <kendo:dropDownList name="referee" id='referee' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#referee").kendoDropDownList({
										    dataSource: [
										        { refereeName: "Referee 1", refereeId: 1 },
										        { refereeName: "Referee 2", refereeId: 2 },
										        { refereeName: "Referee 3", refereeId: 3 },
										        { refereeName: "Referee 4", refereeId: 4 }
										      ],
										      dataTextField: "refereeName",
										      dataValueField: "refereeId",
										      optionLabel: "--Select Referee--",
										      optionLabelTemplate:'<span style="color:black">--Select Referee--</span>'
										  });
									</script>
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="patient" class="col-sm-2 col-form-label">Patient</label>
                                <div class="col-sm-2">
                                <kendo:dropDownList name="identification" id='identification' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#identification").kendoDropDownList({
										    dataSource: [
										        { identificationName: "ID" },
										        { identificationName: "Name"},
										        { identificationName: "Phone"},
										        { identificationName: "HP" },
										        { identificationName: "DOB"},
										        { identificationName: "Address"},
										        { identificationName: "email"}
										      ],
										      dataTextField: "identificationName",
										      dataValueField: "identificationName",
										      optionLabel: "--Please Select--",
										      optionLabelTemplate:'<span style="color:black">--Please Select--</span>'
										  });
									</script>
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="patient" name="identification_patient">
                                </div>
                                <div class="col-sm-3">
                                    <button class='btn btn-info'><a href='' class="text-dark">Search</a></button>
                                    <button class='btn btn-info'><a href='' class="text-dark">OL</a></button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="patientid" class="col-sm-2 col-form-label">Patient ID</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="patientid" name="patientid">
                                </div>
                                <label for="alive" class="col-form-label">Alive</label>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <input type="radio" aria-label="Radio button for following text input">
                                            </div>
                                        </div>
                                        <input type="text" readonly value='YES' class="form-control" aria-label="Text input with radio button">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="patientname" class="col-sm-2 col-form-label">Patient Name</label>
                                <div class="col-sm-2">
                                    <kendo:dropDownList name="first_title" id='first_title' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#first_title").kendoDropDownList({
										    dataSource: [
										        { first_titleName: "Bp" },
										        { first_titleName: "Ibu"},
										        { first_titleName: "Sdr."},
										        { first_titleName: "Sdri."}
										      ],
										      dataTextField: "first_titleName",
										      dataValueField: "first_titleName",
										      optionLabel: "--Please Select--",
										      optionLabelTemplate:'<span style="color:black">--Please Select--</span>'
										  });
									</script>
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="patientname" name="patientname">
                                </div>
                                <div class="col-sm-2">
                                    <kendo:dropDownList name="last_title" id='last_title' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#last_title").kendoDropDownList({
										    dataSource: [
										        { last_titleName: "Gelar 1" },
										        { last_titleName: "Gelar 2"},
										        { last_titleName: "Gelar 3"},
										        { last_titleName: "Gelar 4"}
										      ],
										      dataTextField: "last_titleName",
										      dataValueField: "last_titleName",
										      optionLabel: "--Please Select--",
										      optionLabelTemplate:'<span style="color:black">--Please Select--</span>'
										  });
									</script>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="placeofbirth" class="col-sm-2 col-form-label">Place of Birth</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="placeofbirth" name='placeofbirth'>
                                </div>
                                <label for="dateofbirth" class="col-sm-2 col-form-label">Date of Birth</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="date"  id="datebirth" name='datebirth'>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="city" class="col-sm-2 col-form-label">City</label>
                                <div class="col-sm-2">
                                <kendo:dropDownList name="city" id='city' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#city").kendoDropDownList({
										    dataSource: [
										        { cityName: "City 1" },
										        { cityName: "City 2"},
										        { cityName: "City 3"},
										        { cityName: "City 4"}
										      ],
										      dataTextField: "cityName",
										      dataValueField: "cityName",
										      optionLabel: "--Select City--",
										      optionLabelTemplate:'<span style="color:black">--Select City--</span>'
										  });
									</script>
                            </div>
                                <label for="zipcode" class="col-sm-2 col-form-label">Zipcode</label>
                                <div class="col-sm-2">
                                    <kendo:dropDownList name="zipcode" id='zipcode' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#zipcode").kendoDropDownList({
										    dataSource: [
										        { zipcodeName: "Zipcode 1" },
										        { zipcodeName: "Zipcode 2"},
										        { zipcodeName: "Zipcode 3"},
										        { zipcodeName: "Zipcode 4"}
										      ],
										      dataTextField: "zipcodeName",
										      dataValueField: "zipcodeName",
										      optionLabel: "--Select Zipcode--",
										      optionLabelTemplate:'<span style="color:black">--Select Zipcode--</span>'
										  });
									</script>
                                </div>
                                <label for="country" class="col-sm-2 col-form-label">Country</label>
                                <div class="col-sm-2">
                                    <kendo:dropDownList name="country" id='country' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#country").kendoDropDownList({
										    dataSource: [
										        { countryName: "Country 1" },
										        { countryName: "Country 2"},
										        { countryName: "Country 3"},
										        { countryName: "Country 4"}
										      ],
										      dataTextField: "countryName",
										      dataValueField: "countryName",
										      optionLabel: "--Select Country--",
										      optionLabelTemplate:'<span style="color:black">--Select Country--</span>'
										  });
									</script>
                                </div>
                            </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-4">
                                        <input type="email" class="form-control" id="email" name="email"></div>
                                        <label for="nik" class="col-sm-2 col-form-label">NIK/NPW</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" id="nik" name="nik"></div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="phone" name="phone"></div>
                                        <label for="Fax" class="col-sm-2 col-form-label">Fax</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" id="fax" name="fax">
                                        </div> <label for="jobtitle" class="col-sm-2 col-form-label">Job Title</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" id="jobtitle"></div>    
                                </div>
                                <div class="form-group row">
                                    <label for="workphone" class="col-sm-2 col-form-label">Work Phone</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="workphone" name="workphone">
                                    </div>
                                        <label for="workFax" class="col-sm-2 col-form-label">Work Fax</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" id="workfax" name="workfax">
                                        </div> 
                                        <label for="dept" class="col-sm-2 col-form-label">Dept</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" id="dept" name='dept'>
                                        </div>    
                                </div>
                                <div class="form-group row">
                                    <label for="hp" class="col-sm-2 col-form-label">HandPhone</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="hp" name='hp'>
                                    </div>
                                        <label for="division" class="col-sm-2 col-form-label">Division</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" id="division" name="division">
                                        </div>
                                </div>
                                <div class="form-group row">
                                    <label for="maritalstatus" class="col-sm-2 col-form-label">Marital Status</label>
                                    <div class="col-sm-2">
                                        <kendo:dropDownList name="maritalstatus" id='maritalstatus' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#maritalstatus").kendoDropDownList({
										    dataSource: [
										        { maritalstatusName: "Single" },
										        { maritalstatusName: "Married"},
										        { maritalstatusName: "Divorced"},
										        { maritalstatusName: "Prefer Not Say"}
										      ],
										      dataTextField: "maritalstatusName",
										      dataValueField: "maritalstatusName",
										      optionLabel: "--Marital Status--",
										      optionLabelTemplate:'<span style="color:black">--Marital Status--</span>'
										  });
									</script>
                                    </div>
                                    <label for="religion" class="col-sm-2 col-form-label">Religion</label>
                                    <div class="col-sm-2">
                                    <kendo:dropDownList name="religion" id='religion' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#religion").kendoDropDownList({
										    dataSource: [
										        { religionName: "Islam" },
										        { religionName: "Budha"},
										        { religionName: "Hindu"},
										        { religionName: "Kristen" },
										        { religionName: "Katolik"},
										        { religionName: "Kong Hu Chu"},
										        { religionName: "Prefer Not Say"}
										      ],
										      dataTextField: "religionName",
										      dataValueField: "religionName",
										      optionLabel: "--Religion--",
										      optionLabelTemplate:'<span style="color:black">--Religion--</span>'
										  });
									</script>
                                    </div>
                                    <label for="location" class="col-sm-2 col-form-label">Location/Side</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="location" name='location'>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="occupation" class="col-sm-2 col-form-label">Occupation</label>
                                    <div class="col-sm-2">
                                        <kendo:dropDownList name="occupation" id='occupation' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#occupation").kendoDropDownList({
										    dataSource: [
										        { occupationName: "Occupation 1" },
										        { occupationName: "Occupation 2"},
										        { occupationName: "Occupation 3"},
										        { occupationName: "Occupation 4"}
										      ],
										      dataTextField: "occupationName",
										      dataValueField: "occupationName",
										      optionLabel: "--Occupation--",
										      optionLabelTemplate:'<span style="color:black">--Occupation--</span>'
										  });
									</script>
                                    </div>
                                    <label for="education" class="col-sm-2 col-form-label">Education</label>
                                    <div class="col-sm-2">
                                         <kendo:dropDownList name="education" id='education' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#education").kendoDropDownList({
										    dataSource: [
										        { educationName: "Education 1" },
										        { educationName: "Education 2"},
										        { educationName: "Education 3"},
										        { educationName: "Education 4"}
										      ],
										      dataTextField: "educationName",
										      dataValueField: "educationName",
										      optionLabel: "--Education--",
										      optionLabelTemplate:'<span style="color:black">--Education--</span>'
										  });
									</script>
                                    </div>
                                    <label for="sap" class="col-sm-2 col-form-label">SAP#</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="sap" name='sap'>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="mothersname" class="col-sm-2 col-form-label">Mother's Name</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="mothersname" name='mothersname'>
                                    </div>
                                    <label for="ethnic" class="col-sm-2 col-form-label">Ethnic</label>
                                    <div class="col-sm-2">
                                        <kendo:dropDownList name="ethnic" id='ethnic' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#ethnic").kendoDropDownList({
										    dataSource: [
										        { ethnicName: "Ethnic 1" },
										        { ethnicName: "Ethnic 2"},
										        { ethnicName: "Ethnic 3"},
										        { ethnicName: "Ethnic 4"}
										      ],
										      dataTextField: "ethnicName",
										      dataValueField: "ethnicName",
										      optionLabel: "--Ethnic--",
										      optionLabelTemplate:'<span style="color:black">--Ethnic--</span>'
										  });
									</script>
                                    </div>
                                    <label for="hireddate" class="col-sm-2 col-form-label">Hired Date</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="hireddate" name="hireddate">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="otherid" class="col-sm-2 col-form-label">Other's ID/Cent</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="othersid" name="othersid">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="note" class="col-sm-2 col-form-label">Note</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control"rows="3" name="note" id='note'></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="alert" class="col-sm-2 col-form-label">Alert</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="alert" name="alert">
                                    </div>
                                </div>
                        </div>
                    </div> 
                </div> <div class='col-sm-3 text-left'>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="outlet">Outlet</label>
                                <input type="text" class="form-control text-center" id="outlet" value='Cideng' disabled>
                            </div>
                            <div class="form-group row">
                                <div class='col-sm-4-md mx-auto' align='left'>
                                    <button class='btn btn-success'><a href="index.jsp" class="text-white">New ID</a></button>
                                </div>
                                 <div class='col-sm-4-md mx-auto' align='center'>
                                    <button class='btn btn-secondary'><a href="index.jsp" class="text-white">Edit</a></button>
                                </div>
                                <div class="col-sm-4-md mx-auto" align='right'>
                                   <button class='btn btn-success'><a href="#" class="text-white">Refresh</a></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="queue">Queue</label>
                                <textarea class="form-control" id="queue" rows="3" name="queue"></textarea>
                            </div>
                            <div class='card'>
                            <div class='card-header text-center'>
                            Membership
                            </div>
                            <div class='card-body'>
                            <div class="table-responsive">
                            <table class="card-table table table-bordered table-secondary" id='membership_table'>
    						<thead>
      						<tr>
        					<th scope="col">MEMBERSHIP</th>
       						<th scope="col">RELATIONSHIP</th>
        					<th scope="col">MEMBERSHIP_NO</th>
        					<th scope="col">BEGIN_DATE</th>
      						</tr>
    						</thead>
    						<tbody>
    						</tbody>
  							</table>
  							<script>
        						$(document).ready(function() {
        						    $('#membership_table').DataTable( {
        						        "scrollY": 200,
        						        "scrollX": true
        						    } );
        						} );
								</script>
                            </div>
                            </div>
                            </div>
                            <br>
                            <div class='card'>
                            <div class='card-header text-center'>
                            Registered
                            </div>
                            <div class='card-body'>
                            
                            <div class="table-responsive">
                            <table class="card-table table table-bordered table-secondary" id='registered_table'>
    						<thead>
      						<tr>
        					<th scope="col">REG_NO</th>
       						<th scope="col">REG_DATE</th>
        					<th scope="col">NAME</th>
        					<th scope="col">OUTLET</th>
      						</tr>
    						</thead>
    						<tbody>
    						</tbody>
  							</table>
  							<script>
        						$(document).ready(function() {
        						    $('#registered_table').DataTable( {
        						        "scrollY": 200,
        						        "scrollX": true
        						    } );
        						} );
								</script>
                            </div>
                            </div>
                            </div>
                            </div>
                            <br><br>
                                <div class="form-group row">
                                    <div class="col-sm-12" align='center'>
                                        <button class='btn btn-warning text-dark'>Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
                            
       <br> <jsp:include page="footer.jsp"/>
</body>
</html>