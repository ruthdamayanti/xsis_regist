<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Requisition</title>
 <!-- telerik files -->
<link href="styles/kendo.common-bootstrap.min.css" rel="stylesheet">
<link href="styles/kendo.default.min.css" rel="stylesheet">
<!-- Data Table -->
<link href="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" rel="stylesheet">
<!-- jquery and kendo javascript. jquery MUST be first. -->
<script src="js/jquery.min.js"></script>
<script src="js/kendo.all.min.js"></script>
	<link rel="icon" href="image/prodiafavicon.ico" type="image/gif" sizes="16x16">
</head>
<body>
 <jsp:include page="header.jsp"/><br><br>
             <div class="container-fluid text-center" >
            <div class="row content">
                <div class="col-sm-10 text-left  mx-auto ">
                    <div class='card'>
                        <div class="card-body">
                            <form>
                            <div class="form-group row">
                                    <div class="custom-control custom-checkbox col-form-label">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">HS</label>
                                    </div>
                                <label for="noreg" class="col-sm-1 col-form-label">Reg.No</label>
                                <div class="col-2">
                                    <input type="text" class="form-control"name='referee_no' id='referee_no' placeholder='No Referee'>
                                </div>
                                <div class="col-2">
                                		<input type='date' class='form-control' name='date' id='date'>
                					   
                                </div>
                                <div class="col-2">
                                    <input type="text" class="form-control" name='patientname' id='patientname' placeholder='Patient Name'>
                                </div>
                                <div class="col-2">
                                    <input type="text" class="form-control" name='noreg' id=noreg' placeholder='Registration Number'>
                                </div>
                                
                                <div class="col-2">
                                    <input type="text" class="form-control" name='refereename' id='refereename' placeholder='Referee Name'>
                                </div>
                        </div>
                    </div>
                </div>
                </div><br>
         </div>
         </div><br>
         <div class="container-fluid text-center">
            <div class="row content justify-content-md-center">
                <div class="col-sm-6 text-left">
                    <div class='card'>
                        <div class='card-header'>
                            <b>Doctor Form</b><br><br>
                            <div class="form-group row mx-auto"><button class='btn btn-info'><a href='' class="text-dark form-label">New Form</a></button>
                               <div class="col-sm-2">
                                <kendo:dropDownList name="identification_doctor" id='identification_doctor' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#identification_doctor").kendoDropDownList({
										    dataSource: [
										        { identification_doctorName: "ID" },
										        { identification_doctorName: "Name"}
										      ],
										      dataTextField: "identification_doctorName",
										      dataValueField: "identification_doctorName",
										      optionLabel: "Select",
										      optionLabelTemplate:'<span style="color:black">Select</span>'
										  });
									</script>
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="patient" name="identification_patient">
                                </div>
                                <div class="col-sm-2" align='right'>
                                    <button class='btn btn-info'><a href='' class="text-dark">Search</a></button>
                                </div>
                                <div class="col-sm-1">
                                    <button class='btn btn-info'><a href='' class="text-dark">OL</a></button>
                                </div>
                            </div>
                             <div class="form-group row">
                                <label for="doctor_name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-4">
                                <input type="text" class="form-control" id="doctor_name" name="doctor_name">
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="patient" name="identification_patient">
                                </div>
                                <div class="col-sm-2">
                                    <button class='btn btn-info'><a href='' class="text-dark">New</a></button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="doctor_address" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                <textarea class="form-control"rows="3" name="doctor_address" id='doctor_address'></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                    <label for="phone_doctor" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="phone_doctor" name='phone_doctor'>
                                    </div>
                                        <label for="fax_doctor" class="col-sm-2 col-form-label">Fax</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" id="fax_doctor" name="fax_doctor">
                                        </div>
                                </div>
                            <div class="form-group row">
                                    <label for="from_rujukan" class="col-sm-2 col-form-label">From</label>
                                    <div class="col-sm-4">
                                        <kendo:dropDownList name="from_rujukan" id='from_rujukan' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#from_rujukan").kendoDropDownList({
										    dataSource: [
										        { from_rujukanName: "Prodia" },
										        { from_rujukanName: "CITO"},
										        {form_rujukanName: 'Pramita'}
										      ],
										      dataTextField: "from_rujukanName",
										      dataValueField: "from_rujukanName",
										      optionLabel: "Select",
										      optionLabelTemplate:'<span style="color:black">Select</span>'
										  });
									</script>
                                    </div>
                                        <label for="date_doctor_form" class="col-sm-2 col-form-label">Date</label>
                                        <div class="col-sm-4">
                                            <input type="date" class="form-control" id="date_doctor_form" name="date_doctor_form">
                                        </div>
                                </div>
                            <div class="form-group row">
                                    <label for="result" class="col-sm-2 col-form-label">Result Report</label>
                                    <div class='col-sm-2'>
                                     <input id="numeric_result" type="number" title="numeric_result" value="1" min="0" max="100" step="1" style="width: 100%;"  class='form-control md-auto'>
                                     <script>
                                     $(document).ready(function() {
                                         // create NumericTextBox from input HTML element
                                         $("#numeric_result").kendoNumericTextBox();
                                     });
                                     </script>
                                    </div>
                                    <div class="col-sm-2">
                                        <kendo:dropDownList name="result" id='result' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#result").kendoDropDownList({
										    dataSource: [
										        { resultName: "Option 1"},
										        { resultName: "Option 2"}
										      ],
										      dataTextField: "resultName",
										      dataValueField: "resultName",
										      optionLabel: "--Options--",
										      optionLabelTemplate:'<span style="color:black">--Options--</span>'
										  });
									</script>
                                    </div>
                                        <label for="copy" class="col-sm-2 col-form-label">Copy</label>
                                        <div class='col-sm-2'>
                                     <input id="numeric_copy" type="number" title="numeric_copy" value="1" min="0" max="100" step="1" style="width: 100%;" class='form-control md-auto'>
                                     <script>
                                     $(document).ready(function() {
                                         // create NumericTextBox from input HTML element
                                         $("#numeric_copy").kendoNumericTextBox();
                                     });
                                     </script>
                                    </div>
                                        <div class="col-sm-2">
                                             <kendo:dropDownList name="copy" id='copy' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#copy").kendoDropDownList({
										    dataSource: [
										        { copyName: "Option 1"},
										        { copyName: "Option 2"}
										      ],
										      dataTextField: "copyName",
										      dataValueField: "copyName",
										      optionLabel: "--Options--",
										      optionLabelTemplate:'<span style="color:black">--Options--</span>'
										  });
									</script>
                                        </div>
                                </div>
                            <div class="form-group row">
                                    <label for="result_report" class="col-sm-2 col-form-label">Result Report</label>
                                    <div class="col-sm-4">
                                        <kendo:dropDownList name="result_report" id='result_report' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#result_report").kendoDropDownList({
										    dataSource: [
										        { result_reportName: "Indonesia-Prodia Unit"},
										        { result_reportName: "English-S1 Unit"}
										      ],
										      dataTextField: "result_reportName",
										      dataValueField: "result_reportName",
										      optionLabel: "--Options--",
										      optionLabelTemplate:'<span style="color:black">--Options--</span>'
										  });
									</script>
                                    </div>
                                        <label for="copy_result" class="col-sm-2 col-form-label">Copy</label>
                                        <div class="col-sm-4">
                                             <kendo:dropDownList name="copy_result" id='copy_result' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#copy_result").kendoDropDownList({
										    dataSource: [
										        { copy_resultName: "Indonesia-Prodia Unit"},
										        { copy_resultName: "English-S1 Unit"}
										      ],
										      dataTextField: "copy_resultName",
										      dataValueField: "copy_resultName",
										      optionLabel: "--Options--",
										      optionLabelTemplate:'<span style="color:black">--Options--</span>'
										  });
									</script>
                                        </div>
                                </div>
                                 <div class="form-group row">
                                        <label for="fax_doctor" class="col-sm-2 col-form-label">Fax</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" id="fax_doctor" name="fax_doctor">
                                        </div>
                                    <label for="phone_doctor" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="phone_doctor" name='phone_doctor'>
                                    </div>
                                </div>
                            <div class="form-group row">
                                <label for="doctor_diagnosis" class="col-sm-2 col-form-label">Diagnosis</label>
                                <div class="col-sm-10">
                                <textarea class="form-control"rows="3" name="doctor_diagnosis" id='doctor_diagnosis'></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="doctor_notes" class="col-sm-2 col-form-label">Notes</label>
                                <div class="col-sm-10">
                                <textarea class="form-control"rows="3" name="doctor_notes" id='doctor_notes'></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                           		</div>
                                <label for="doctor_notes" class="col-sm-2 col-form-label">Ref</label>
                                <div class='col-sm-4'>
                                	<input type="text" class="form-control" id="ref" name='ref'>
                                </div>
                            </div>
                            
                            <div class="form-group row mx-auto">
                            <div class="col-sm-10" align='left'>
                            <button class='btn btn-secondary'><a href='' class="text-dark form-label">Add</a></button>
                                <button class='btn btn-secondary'><a href='' class="text-dark">Edit</a></button>
                                    <button class='btn btn-secondary'><a href='' class="text-dark">Delete</a></button>
                                <button class='btn btn-danger'><a href='' class="text-dark">Cancel</a></button>
                            </div>
                                <div class="col-sm-2"align='right'>
                                    <button class='btn btn-success'><a href='' class="text-dark">Save</a></button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                           <div class="form-group row">
                                <label for="test" class="col-sm-2 col-form-label">Test</label>
                                <div class="col-sm-8">
                                    <kendo:dropDownList name="test" id='test' style="width: 100%;">
        							<kendo:dataSource data="${data}"></kendo:dataSource>
    								</kendo:dropDownList>  
									<script>
										$("#test").kendoDropDownList({
										    dataSource: [
										        { testName: "Test 1"},
										        { testName: "Test 2"},
										        { testName: "Test 3" },
										        { testName: "Test 4"}
										      ],
										      dataTextField: "testName",
										      dataValueField: "testName",
										      optionLabel: "--Choose Test--",
										      optionLabelTemplate:'<span style="color:black">--Choose Test--</span>'
										  });
									</script>
                                </div>
                                <div class="col-sm-2">
                                    <button class='btn btn-success'><a href='#'class='text-dark'>+</a></button>
                                    <button class='btn btn-success'><a href='#'class='text-dark'>-</a></button>
                                </div><br><br><br>
                                 <div class='col-sm-12'>
                                 
                            <div class="table-responsive">
                            <table class="card-table table table-bordered table-secondary" id='test_table'>
    						<thead>
      						<tr>
        					<th scope="col">TEST</th>
       						<th scope="col">PRICE</th>
        					<th scope="col">DISCOUNT</th>
        					<th scope="col">NET</th>
        					<th scope="col">PANEL</th>
      						</tr>
    						</thead>
    						<tbody>
    						</tbody>
  							</table>
  							<script>
        						$(document).ready(function() {
        						    $('#test_table').DataTable( {
        						        "scrollY": 200,
        						        "scrollX": true
        						    } );
        						} );
								</script>
                            </div>
                            </div><br><br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-left px-md-2">
                    <div class='card'>
                        <div class='card-header'>
                            Sender
                        </div>
                        <div class="card-body">
                            <div class='col-sm-12'>
                            <div class="table-responsive">
                            <table class="card-table table table-bordered table-secondary" id='sender_table'>
    						<thead>
      						<tr>
        					<th scope="col">ID</th>
       						<th scope="col">NAME</th>
        					<th scope="col">FORM</th>
        					<th scope="col">R</th>
        					<th scope="col">RESULT_TO</th>
       						<th scope="col">C</th>
        					<th scope="col">COPY_TO</th>
        					<th scope="col">DIAGNOSIS</th>
        					<th scope="col">NOTE</th>
       						<th scope="col">REF_NO</th>
        					<th scope="col">FAX</th>
        					<th scope="col">PHONE</th>
        					<th scope="col">ADDRESS</th>
       						<th scope="col">DOCTOR_PHONE</th>
        					<th scope="col">DOCTOR_FAX</th>
        					<th scope="col">USER_UPDATE</th>
        					<th scope="col">USER_DATE</th>
      						</tr>
    						</thead>
    						<tbody>
    						</tbody>
  							</table>
  							<script>
        						$(document).ready(function() {
        						    $('#sender_table').DataTable( {
        						        "scrollY": 200,
        						        "scrollX": true
        						    } );
        						} );
								</script>
                            </div>	
                            </div><br><br>
                        </div>
                         <div class="card-footer">
                             <button class='btn btn-warning'><a href='#' class='text-dark'>Add</a></button>
                        </div>
                    </div>
                </div>
                </div>
                
         </div><br>
         <div class="container-fluid text-center" >
            <div class="row content">
                <div class="col-sm-10 text-left  mx-auto ">
                    <div class='card'>
                        <div class='card-header text-center'>
                            Conditition
                        </div>
                        <div class="card-body">
                            <div class='col-sm-12'>
                                <div class="table-responsive">
                            <table class="card-table table table-bordered table-secondary" id='conditition_table'>
    						<thead>
      						<tr>
        					<th scope="col">ID</th>
       						<th scope="col">NAME</th>
        					<th scope="col">CONDITITION</th>
        					<th scope="col">NOTE</th>
      						</tr>
    						</thead>
    						<tbody>
    						</tbody>
  							</table>
  							<script>
        						$(document).ready(function() {
        						    $('#conditition_table').DataTable( {
        						        "scrollY": 200,
        						        "scrollX": true
        						    } );
        						} );
								</script>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-secondary"><a href='#' class="text-light">Next</a></button>
                        </div>
                    </div>
                </div>
                </div><br>  
         </div>
         </form>
         <br>
         </div>
         <jsp:include page="footer.jsp"/>
</body>
</html>